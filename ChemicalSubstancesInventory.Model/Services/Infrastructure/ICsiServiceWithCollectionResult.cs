﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public interface ICsiServiceWithCollectionResult<T> : ICsiService<CsiServiceCollectionResult<T>>
    {

    }
}
