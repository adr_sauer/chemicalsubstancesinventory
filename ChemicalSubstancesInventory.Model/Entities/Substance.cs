﻿using ChemicalSubstancesInventory.Model.Enums;
using ChemicalSubstancesInventory.Model.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class Substance
    {
        public class SubstanceByPKComparer : IEqualityComparer<Substance>
        {
            public bool Equals(Substance x, Substance y)
            {
                if (x == null && y == null)
                    return true;
                else if (x == null | y == null)
                    return false;
                else if (x.SubstanceId == y.SubstanceId)
                    return true;
                else
                    return false;
            }

            public int GetHashCode(Substance obj)
            {
                return obj.SubstanceId.GetHashCode();
            }
        }

        public int SubstanceId { get; set; }

        [Display(Name = "Substance_IupacName_Display", ResourceType = typeof(ModelResources))]
        [Required(ErrorMessageResourceName = "Substance_IupacName_Required", ErrorMessageResourceType = typeof(ModelResources))]
        [MaxLength(1000)]
        [StringLength(1000, MinimumLength = 1, ErrorMessageResourceName = "Substance_IupacName_StringLength", ErrorMessageResourceType = typeof(ModelResources))]
        public string IupacName { get; set; }

        [Display(Name = "Substance_CasNumber_Display", ResourceType = typeof(ModelResources))]
        [MaxLength(30)]
        [MustBeValidCasNumber(ErrorMessageResourceName = "Substance_CasNumber_MustBeValidCasNumber", ErrorMessageResourceType = typeof(ModelResources))]
        public string CasNumber { get; set; }

        [Display(Name = "Substance_EcNumber_Display", ResourceType = typeof(ModelResources))]
        [MaxLength(30)]
        [MustBeValidEcNumber(ErrorMessageResourceName = "Substance_EcNumber_MustBeValidEcNumber", ErrorMessageResourceType = typeof(ModelResources))]
        public string EcNumber { get; set; }

        [Display(Name = "Substance_GhsSignalWordEnumValue_Display", ResourceType = typeof(ModelResources))]
        public GhsSignalWordEnum GhsSignalWordEnumValue { get; set; }

        [Display(Name = "Substance_GhsHazardPictograms_Display", ResourceType = typeof(ModelResources))]
        public virtual ICollection<GhsHazardPictogram> GhsHazardPictograms { get; set; }

        public virtual ICollection<GhsHazardStatement> GhsHazardStatements { get; set; }

        public virtual ICollection<GhsPrecautionaryStatement> GhsPrecautionaryStatements { get; set; }

        public virtual ICollection<HazardClassCategory> HazardClassCategories { get; set; }

        public virtual ICollection<MixtureComponent> MixtureComponents { get; set; }



        //public void CopyValuesIntoInstance(Substance destination)
        //{
        //    destination.IupacName = this.IupacName;
        //    destination.CasNumber = this.CasNumber;
        //    destination.EcNumber = this.EcNumber;
        //    destination.GhsSignalWordEnumValue = this.GhsSignalWordEnumValue;


        //    ////destination.GhsHazardStatements.i
        //    //destination.GhsHazardPictograms = this.GhsHazardPictograms;

        //    //foreach (var item in this.GhsHazardStatements)
        //    //{
        //    //    var oldItem = destination.GhsHazardStatements.Where(q => q.GhsHazardStatementCode == item.GhsHazardStatementCode).SingleOrDefault();
        //    //    if (oldItem == null)
        //    //    {
        //    //        destination.GhsHazardStatements.Add(item);
        //    //    }
        //    //}
        //    ////destination.GhsHazardStatements = this.GhsHazardStatements;
        //    //destination.GhsPrecautionaryStatements = this.GhsPrecautionaryStatements;
        //    //destination.HazardClassCategories = this.HazardClassCategories;
        //    //destination.MixtureComponents = this.MixtureComponents;

        //}

        public void UpdateFromInstance(Substance source)
        {
            this.IupacName = source.IupacName;
            this.CasNumber = source.CasNumber;
            this.EcNumber = source.EcNumber;
            this.GhsSignalWordEnumValue = source.GhsSignalWordEnumValue;
            UpdateGhsHazardStatementList(source.GhsHazardStatements);
        }

        private void UpdateGhsHazardStatementList(ICollection<GhsHazardStatement> newList)
        {
            var deletedItems = this.GhsHazardStatements.Except(newList, new GhsHazardStatement.GhsHazardStatementByPKComparer()).ToList();

            var addedItems = newList.Except(this.GhsHazardStatements, new GhsHazardStatement.GhsHazardStatementByPKComparer()).ToList();
            
            deletedItems.ForEach(q => this.GhsHazardStatements.Remove(q));

            addedItems.ForEach(q => this.GhsHazardStatements.Add(q));
        }

    }

    
}
