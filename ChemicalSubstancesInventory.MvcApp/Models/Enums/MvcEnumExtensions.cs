﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChemicalSubstancesInventory.Model.Enums;

namespace ChemicalSubstancesInventory.MvcApp.Models.Enums
{
    public static class MvcEnumExtensions
    {     
        public static SelectList GetLocalisedSelectList(this Enum enumValue)
        {
            var r = Enum.GetValues(enumValue.GetType());
            var values = (from Enum t in r select new { Id = t, Name=t.GetDescription()}).ToList();
            return new SelectList(values, "Id", "Name", enumValue);
        }
    }
}