﻿using ChemicalSubstancesInventory.Model;
using ChemicalSubstancesInventory.Model.Enums;
using ChemicalSubstancesInventory.Model.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChemicalSubstancesInventory.MvcApp.Models.ViewModels
{
    public class SubstanceEditViewModel
    {

        [Required]
        public int SubstanceId { get; set; }

        [Display(Name = "Substance_IupacName_Display", ResourceType = typeof(ModelResources))]
        [Required(ErrorMessageResourceName = "Substance_IupacName_Required", ErrorMessageResourceType = typeof(ModelResources))]
        [MaxLength(1000)]
        [StringLength(1000, MinimumLength = 1, ErrorMessageResourceName = "Substance_IupacName_StringLength", ErrorMessageResourceType = typeof(ModelResources))]
        public string IupacName { get; set; }

        [Display(Name = "Substance_CasNumber_Display", ResourceType = typeof(ModelResources))]
        [MaxLength(30)]
        [MustBeValidCasNumber(ErrorMessageResourceName = "Substance_CasNumber_MustBeValidCasNumber", ErrorMessageResourceType = typeof(ModelResources))]
        public string CasNumber { get; set; }

        [Display(Name = "Substance_EcNumber_Display", ResourceType = typeof(ModelResources))]
        [MaxLength(30)]
        [MustBeValidEcNumber(ErrorMessageResourceName = "Substance_EcNumber_MustBeValidEcNumber", ErrorMessageResourceType = typeof(ModelResources))]
        public string EcNumber { get; set; }

        [Display(Name = "Substance_GhsSignalWordEnumValue_Display", ResourceType = typeof(ModelResources))]
        public GhsSignalWordEnum GhsSignalWordEnumValue { get; set; }

        [Display(Name = "Substance_GhsHazardPictograms_Display", ResourceType = typeof(ModelResources))]
        public List<SubstanceEditViewModelPictogram> Pictograms { get; set; }


        //public SelectList SignalWordEnumSelectList { get; set; }
    }

    public class SubstanceEditViewModelPictogram
    {
        [Required]
        public int GhsHazardPictogramId { get; set; }

        [Required]
        public bool Chosen { get; set; }

        public string GhsPictogramCode { get; set; }     

        public string Name { get; set; }
    }
}