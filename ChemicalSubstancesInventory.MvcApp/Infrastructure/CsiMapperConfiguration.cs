﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using AutoMapper;
using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Services.Dto;
using ChemicalSubstancesInventory.MvcApp.Models.ViewModels;

namespace ChemicalSubstancesInventory.MvcApp.Infrastructure
{
    public class CsiMapperConfiguration : MapperConfiguration
    {
        private static Action<IMapperConfigurationExpression> mapperConfigurationExpression = cfg =>
        {
            cfg.CreateMap<GhsHazardPictogram, SubstanceEditViewModelPictogram>().ForMember(destination => destination.Name, map => map.ResolveUsing(source => {
                    string currentLanguageCode = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
                    GhsHazardPictogramTranslation translation = source.GhsHazardPictogramTranslations.Where(q => string.Compare(q.LanguageCode, currentLanguageCode, true) == 0).SingleOrDefault();
                    return translation.Translation;
                }));

            cfg.CreateMap<Substance, SubstanceEditViewModel>().ForMember(destination => destination.Pictograms, map => map.MapFrom(
                source => new List<SubstanceEditViewModelPictogram>()                
                ));

            //TODO Change in view model and delete formember
            cfg.CreateMap<SubstanceEditorPictogramDto, SubstanceEditViewModelPictogram>().ForMember(dest => dest.Name, map => map.MapFrom(src=>src.TranslatedDescription));

            
            cfg.CreateMap<SubstanceEditorDto, SubstanceEditViewModel>().ForMember(dest => dest.Pictograms, map => map.MapFrom(src => src.PictogamsChoiceList));


        };

        public CsiMapperConfiguration() : base (mapperConfigurationExpression)
        {
            
        }
    }
}