﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.MvcApp.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChemicalSubstancesInventory.MvcApp.Controllers
{
    public partial class TestSubstanceController : Controller
    {
        // GET: TestSubstance
        public string Index()
        {
            return "Index";
        }

        public virtual ActionResult List()
        {
            TestSubstanceListViewModel viewModel = null;
            using (DbContext context = new ChemicalInventoryContext())
            {
                var list = context.Set<Substance>().ToList();

                viewModel = new TestSubstanceListViewModel(list);
            }

            return View(viewModel);
        }
    }
}