﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;

namespace ChemicalSubstancesInventory.Model.Enums
{
    /// <summary>
    /// Contains GHS signal words
    /// </summary>
    public enum GhsSignalWordEnum
    {
        /// <summary>
        /// No GHS signal word
        /// </summary>
        [LocalizedDescription("GhsSignalWordEnumNone", typeof(EnumResources))]
        None =0,
        /// <summary>
        /// GHS Warning signal word
        /// </summary>
        [GhsCode("Wng")]
        [LocalizedDescription("GhsSignalWordEnumWarning", typeof(EnumResources))]
        Warning = 1,
        /// <summary>
        /// GHS Danger signal word
        /// </summary>
        [GhsCode("Dgr")]
        [LocalizedDescription("GhsSignalWordEnumDanger", typeof(EnumResources))]
        Danger =2,
    }
}
