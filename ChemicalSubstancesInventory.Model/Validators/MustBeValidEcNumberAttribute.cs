﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Validators
{
    public class MustBeValidEcNumberAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value is string && value != null && new ECNumberValidator().IsValidECNumber(value as string);
        }
    }
}
