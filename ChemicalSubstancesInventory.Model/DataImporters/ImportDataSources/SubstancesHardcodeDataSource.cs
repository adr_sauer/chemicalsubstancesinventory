﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChemicalSubstancesInventory.Model.DataImporters.Interfaces;
using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Enums;

namespace ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources
{
    public class SubstancesHardcodeDataSource : ISubstancesDataSource
    {
        private readonly List<GhsHazardPictogram> _hazardPictograms;
        private readonly List<GhsHazardStatement> _hazardStatements;
        private readonly List<HazardClassCategory> _hazardClassCategories;

        public SubstancesHardcodeDataSource(List<GhsHazardPictogram> hazardPictograms,
            List<GhsHazardStatement> hazardStatements, List<HazardClassCategory> hazardClassCategories)
        {
            this._hazardPictograms = hazardPictograms;
            this._hazardStatements = hazardStatements;
            this._hazardClassCategories = hazardClassCategories;
        }

        public List<Substance> GetSubstances()
        {
            Substance substance = null;
            List<Substance> result = new List<Substance>();

            substance = new Substance()
            {
                IupacName = "hydrogen",
                CasNumber = "1333-74-0",
                EcNumber = "215-605-7",
                GhsSignalWordEnumValue = GhsSignalWordEnum.Danger,
                GhsHazardPictograms = new List<GhsHazardPictogram>(),
                GhsHazardStatements = new List<GhsHazardStatement>(),
                HazardClassCategories = new List<HazardClassCategory>()
            };
            
            substance.GhsHazardPictograms = _hazardPictograms.Where(q => q.GhsPictogramCode == "GHS02" || q.GhsPictogramCode == "GHS04").ToList();
            substance.GhsHazardStatements = _hazardStatements.Where(q => q.GhsHazardStatementCode == "H220").ToList();
            substance.HazardClassCategories = _hazardClassCategories.Where(q => q.HazardCategoryCode == "Flam. Gas 1" || q.HazardCategoryCode == "Press. Gas").ToList();
            
            result.Add(substance);

            substance = new Substance()
            {
                IupacName = "methanol",
                CasNumber = "67-56-1",
                EcNumber = "200-659-6",
                GhsSignalWordEnumValue = GhsSignalWordEnum.Danger,
                GhsHazardPictograms = new List<GhsHazardPictogram>(),
                GhsHazardStatements = new List<GhsHazardStatement>(),
                HazardClassCategories = new List<HazardClassCategory>()
            };

            substance.GhsHazardPictograms = _hazardPictograms.Where(q => q.GhsPictogramCode == "GHS02"
            || q.GhsPictogramCode == "GHS06"
            || q.GhsPictogramCode == "GHS08").ToList();
            substance.GhsHazardStatements = _hazardStatements.Where(q => q.GhsHazardStatementCode == "H225"
            || q.GhsHazardStatementCode== "H331"
            || q.GhsHazardStatementCode == "H311"
            || q.GhsHazardStatementCode == "H301"
            || q.GhsHazardStatementCode == "H370").ToList();
            substance.HazardClassCategories = _hazardClassCategories.Where(q => q.HazardCategoryCode == "Flam. Liq. 2"
            || q.HazardCategoryCode == "Acute Tox. 3"
            || q.HazardCategoryCode == "STOT SE 1").ToList();

            result.Add(substance);

            return result;
        }
    }
}
