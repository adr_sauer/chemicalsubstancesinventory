﻿using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.DataImporters.Interfaces
{
    public interface ISubstancesDataSource
    {
        List<Substance> GetSubstances();
    }
}
