﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Validators
{
    /// <summary>
    /// Provides validation of CAS Registry Numbers
    /// </summary>
    /// <remarks>
    /// A CASRN is separated by hyphens into three parts, the first consisting from two up to seven digits, the second consisting of two digits, and the third consisting of a single digit serving as a check digit. 
    /// This current format gives CAS a maximum capacity of 1,000,000,000 unique identifiers.
    /// The check digit is found by taking the last digit times 1, the previous digit times 2, the previous digit times 3 etc., adding all these up and computing the sum modulo 10. For example, the CAS number of water is 7732-18-5: the checksum 5 is calculated as (8×1 + 1×2 + 2×3 + 3×4 + 7×5 + 7×6) = 105; 105 mod 10 = 5.
    /// </remarks>
    public class CASValidator
    {
        private const string _casRegexPattern = @"^\d{2,7}-\d{2}-\d$";
        private const int _casSegmentsCount = 3;
        private Regex _casRegex;


        /// <summary>
        /// Initializes a new instance of <see cref="CASValidator"/> class.
        /// </summary>
        public CASValidator()
        {
            _casRegex = new Regex(_casRegexPattern, RegexOptions.CultureInvariant);
        }

        /// <summary>
        /// Checks if a given string is a valid CAS number.
        /// </summary>
        /// <param name="casNumber">CAS number string to validate.</param>
        /// <returns>True if argument is valid CAS number. False otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown if argument is null.</exception>
        public bool IsValidCASNumber(string casNumber)
        {
            bool result = true;
            if (casNumber == null)
            {
                throw new ArgumentNullException("casNumber");
            }

            if (_casRegex.IsMatch(casNumber))
            {
                string[] casNumberSegments = casNumber.Split('-');
                int lastDigit = Int32.Parse(casNumberSegments[casNumberSegments.Length - 1]);
                result = (lastDigit == CalculateCheckDigit(casNumberSegments));
            }
            else
            {
                result = false;
            }

            return result;
        }

        private int CalculateCheckDigit(string[] casNumberSegments)
        {
            int[] casDigitsReversed = new int[casNumberSegments[0].Length + casNumberSegments[1].Length];

            int i = 0;
            foreach (char c in casNumberSegments[1].Reverse())
            {
                casDigitsReversed[i] = Int32.Parse(c.ToString());
                i++;
            }

            foreach (char c in casNumberSegments[0].Reverse())
            {
                casDigitsReversed[i] = Int32.Parse(c.ToString());
                i++;
            }

            int sum = 0;
            for (int j = 0, times = 1; j < casDigitsReversed.Length; j++, times++)
            {
                sum += casDigitsReversed[j] * times;
            }

            return sum % 10;
        }
    }
}
