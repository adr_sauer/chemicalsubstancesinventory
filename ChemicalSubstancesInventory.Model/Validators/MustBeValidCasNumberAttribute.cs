﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Validators
{
    public class MustBeValidCasNumberAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value is string && value != null && new CASValidator().IsValidCASNumber(value as string);
        }
    }
}
