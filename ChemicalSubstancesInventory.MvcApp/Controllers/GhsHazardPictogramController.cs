﻿using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ChemicalSubstancesInventory.MvcApp.Controllers
{
    public partial class GhsHazardPictogramController : Controller
    {
        private IChemicalInventoryContext _dbContext;

        public GhsHazardPictogramController(IChemicalInventoryContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual ContentResult SvgString(int? id)
        {
            string result = string.Empty;

            if (id.HasValue)
            {
                GhsHazardPictogram dbPictogram = _dbContext.GhsHazardPictograms.Find(id.Value);
                if (dbPictogram != null)
                {
                    result = dbPictogram.SvgData;
                }

            }

            return Content(result);
        }

        public virtual ContentResult TranslatedNameString(int? id)
        {
            string result = string.Empty;

            if (ModelState.IsValid && id.HasValue)
            {
                try
                {
                    result = GetTranslatedGhsHazardPictogramNameFromDb(id.Value);
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return Content(result);
        }

        private string GetTranslatedGhsHazardPictogramNameFromDb(int id)
        {
            string result = string.Empty;
            var queryResult = _dbContext.GhsHazardPictograms.Find(id);

            if (queryResult != null)
            {
                var translationObject = queryResult.GhsHazardPictogramTranslations.Where(q => q.LanguageCode == Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName).SingleOrDefault();

                if (translationObject != null)
                {
                    result = translationObject.Translation;
                }
                else if (queryResult.GhsPictogramEnglishName != null)
                {
                    result = queryResult.GhsPictogramEnglishName;
                }
            }

            return result;
        }
    }
}