﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public class CsiServiceCollectionResult<ResultType> : CsiServiceResult
    {
        public CsiServiceCollectionResult(ICollection<CsiServiceError> errors, IQueryable<ResultType> result) : base(errors)
        {
            QueryableResult = result;
        }

        public IList<ResultType> Result
        {
            get
            {
                if (QueryableResult != null)
                {
                    return QueryableResult.ToList();
                }
                else
                {
                    return null;
                }
            }
        }

        public IQueryable<ResultType> QueryableResult { get; private set; }
    }
}
