﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.MvcApp.Controllers;
using ChemicalSubstancesInventory.MvcAppTests.Infrastructure.MockDbContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.MvcAppTests.MvcApp.Controllers
{
    [TestClass]
    public class GhsHazardPictogramControllerTests
    {
        private class MockDataSet
        {
            public GhsHazardPictogramTranslation Translation1 { get; set; }
            public GhsHazardPictogramTranslation Translation2 { get; set; }
            public GhsHazardPictogramTranslation Translation3 { get; set; }
            public GhsHazardPictogramTranslation Translation4 { get; set; }
            public GhsHazardPictogram Pictogram1 { get; set; }
            public GhsHazardPictogram Pictogram2 { get; set; }
            public IChemicalInventoryContext MockDbContext { get; set; }
            

            public MockDataSet()
            {
                Translation1 = new GhsHazardPictogramTranslation { GhsHazardPictogramId = 1, LanguageCode = "pl", Translation = "translation1" };
                Translation2 = new GhsHazardPictogramTranslation { GhsHazardPictogramId = 1, LanguageCode = "en", Translation = "translation2" };
                Translation3 = new GhsHazardPictogramTranslation { GhsHazardPictogramId = 2, LanguageCode = "pl", Translation = "translation3" };
                Translation4 = new GhsHazardPictogramTranslation { GhsHazardPictogramId = 2, LanguageCode = "en", Translation = "translation4" };

               Pictogram1 = new GhsHazardPictogram
                {
                    GhsHazardPictogramId = 1,
                    GhsPictogramCode = "code1",
                    GhsPictogramEnglishName = "name1",
                    SvgData = "data1",
                    GhsHazardPictogramTranslations = new List<GhsHazardPictogramTranslation> { Translation1, Translation2 }
                };
                Pictogram2 = new GhsHazardPictogram
                {
                    GhsHazardPictogramId = 2,
                    GhsPictogramCode = "code2",
                    GhsPictogramEnglishName = "name2",
                    SvgData = "data2",
                    GhsHazardPictogramTranslations = new List<GhsHazardPictogramTranslation> { Translation3, Translation4 }
                };

                MockDbContext = new MockChemicalInventoryContext
                {
                    GhsHazardPictogramTranslations = { Translation1, Translation2, Translation3, Translation4 },
                    GhsHazardPictograms = { Pictogram1, Pictogram2 }
                };
            }
        }

        [TestMethod]
        public void SvgStringShould_ReturnValidSvgData_WhenGivenValidId()
        {
            //Arrange
            GhsHazardPictogramController controller = null;
            MockDataSet mockDataSet = null;

            try
            {
                mockDataSet = new MockDataSet();
                IChemicalInventoryContext dbContext = mockDataSet.MockDbContext;

                controller = new GhsHazardPictogramController(dbContext);

                var ci = new CultureInfo("pl");
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            string translation = controller.SvgString(mockDataSet.Pictogram1.GhsHazardPictogramId).Content;

            //Assert
            Assert.IsNotNull(translation);
            Assert.AreEqual(mockDataSet.Pictogram1.SvgData, translation);
        }

        [TestMethod]
        public void SvgStringShould_ReturnEmptyContent_WhenGivenInvalidId()
        {
            //Arrange
            MockDataSet mockDataSet = null;
            GhsHazardPictogramController controller = null;

            try
            {
                mockDataSet = new MockDataSet();
                IChemicalInventoryContext dbContext = mockDataSet.MockDbContext;

                controller = new GhsHazardPictogramController(dbContext);

                var ci = new CultureInfo("pl");
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            string translation = controller.SvgString(3).Content;

            //Assert
            Assert.IsNotNull(translation);
            Assert.AreEqual(string.Empty, translation);
        }

        [TestMethod]
        public void SvgStringShould_ReturnEmptyContent_WhenGivenNullId()
        {
            //Arrange
            MockDataSet mockDataSet = null;
            GhsHazardPictogramController controller = null;

            try
            {
                mockDataSet = new MockDataSet();
                IChemicalInventoryContext dbContext = mockDataSet.MockDbContext;

                controller = new GhsHazardPictogramController(dbContext);

                var ci = new CultureInfo("pl");
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            string translation = controller.SvgString(null).Content;

            //Assert
            Assert.IsNotNull(translation);
            Assert.AreEqual(string.Empty, translation);
        }

        [TestMethod]
        public void TranslatedNameStringShould_ReturnValidTranslationString_ForValidParameters()
        {
            //Arrange
            MockDataSet mockDataSet = null;
            GhsHazardPictogramController controller = null;

            try
            {
                mockDataSet = new MockDataSet();
                IChemicalInventoryContext dbContext = mockDataSet.MockDbContext;

                controller = new GhsHazardPictogramController(dbContext);

                var ci = new CultureInfo("pl");
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            string translation = controller.TranslatedNameString(mockDataSet.Pictogram2.GhsHazardPictogramId).Content;

            //Assert
            Assert.IsNotNull(translation);
            Assert.AreEqual(mockDataSet.Translation3.Translation, translation);
        }


        [TestMethod]
        public void TranslatedNameStringShould_ReturnEnglishName_ForInvalidCurrentCulture()
        {
            //Arrange
            MockDataSet mockDataSet = null;
            GhsHazardPictogramController controller = null;

            try
            {
                mockDataSet = new MockDataSet();
                IChemicalInventoryContext dbContext = mockDataSet.MockDbContext;

                controller = new GhsHazardPictogramController(dbContext);

                var ci = new CultureInfo("nl");
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            string translation = controller.TranslatedNameString(mockDataSet.Pictogram2.GhsHazardPictogramId).Content;

            //Assert
            Assert.IsNotNull(translation);
            Assert.AreEqual(mockDataSet.Pictogram2.GhsPictogramEnglishName, translation);
        }

        [TestMethod]
        public void TranslatedNameStringShould_ReturnEmptyContent_ForInvalidId()
        {
            //Arrange
            MockDataSet mockDataSet = null;
            GhsHazardPictogramController controller = null;

            try
            {
                mockDataSet = new MockDataSet();
                IChemicalInventoryContext dbContext = mockDataSet.MockDbContext;

                controller = new GhsHazardPictogramController(dbContext);

                var ci = new CultureInfo("pl");
                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            string translation = controller.TranslatedNameString(3).Content;

            //Assert
            Assert.IsNotNull(translation);
            Assert.AreEqual(string.Empty, translation);
        }


        

    }
}
