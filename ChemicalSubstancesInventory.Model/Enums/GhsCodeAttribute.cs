﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Enums
{
    /// <summary>
    /// Specifies a GHS code.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class GhsCodeAttribute : Attribute
    {
        private readonly string _ghsCode;

        /// <summary>
        /// Initializes a new instance of the <see cref="GhsCodeAttribute"/> class with a GHS code.
        /// </summary>
        /// <param name="ghsCode">GHS code string.</param>
        public GhsCodeAttribute(string ghsCode)
        {
            _ghsCode = ghsCode;
        }

        /// <summary>
        /// The GHS code stored in this attribute.
        /// </summary>
        public string GhsCode
        {
            get
            {
                return _ghsCode;
            }
        }
    }
}
