﻿using ChemicalSubstancesInventory.Model.Services.Dto;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services
{
    public interface IGetSubstanceEditorDtoService : ICsiServiceWithObjectResult<SubstanceEditorDto>
    {
        int SubstanceId { get; set; }
        string LanguageCode { get; set; }
        //SubstanceEditorDto GetSubstanceEditorDto(int substanceId, string languageCode);
    }
}
