﻿using ChemicalSubstancesInventory.Model.Services;
using ChemicalSubstancesInventory.MvcApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.MvcAppTests.MvcApp
{
    [TestClass]
    public class NinjectResolverTests
    {
        [TestMethod]
        public void Should_Pass_WhenAllServicesAreBinded()
        {
            var resolver = new NinjectResolver();


            var service = resolver.GetService(typeof(IGetGhsHazardPictogramTranslationService));

            Assert.IsNotNull(service);

            service = resolver.GetService(typeof(IGetSubstanceEditorDtoService));

            Assert.IsNotNull(service);

        }
    }
}
