﻿using ChemicalSubstancesInventory.Model.Entities;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Web.Common;
using ChemicalSubstancesInventory.Model.Services.Concrete;
using ChemicalSubstancesInventory.Model.Services;

namespace ChemicalSubstancesInventory.MvcApp
{
    public class NinjectResolver : System.Web.Mvc.IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectResolver()
        {
            _kernel = new StandardKernel();
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<IChemicalInventoryContext>().To<ChemicalInventoryContext>().InRequestScope();
            _kernel.Bind<IGetSubstanceEditorDtoService>().To<GetSubstanceEditorDtoService>();
            _kernel.Bind<IGetSubstancePictogramsChoiceListService>().To<GetSubstancePictogramsChoiceListService>();

            _kernel.Bind <IGetGhsHazardPictogramTranslationService>().To<GetGhsHazardPictogramTranslationService>();
            //_kernel.
            //_kernel.Bind<IChemicalInventoryContext>().ToSelf().InRequestScope();
            // _kernel.Bind<To, From>(); // Registering Types    
        }
    }
}