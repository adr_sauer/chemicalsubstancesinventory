﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class GhsPrecautionaryStatementTranslation
    {
        [Key]
        [Column(Order = 1)]
        public int GhsPrecautionaryStatementId { get; set; }

        [Key]
        [Column(Order = 2)]
        [MaxLength(5)]
        public string LanguageCode { get; set; }

        [Required]
        public string Translation { get; set; }

        public virtual GhsPrecautionaryStatement GhsPrecautionaryStatement { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0}, Language: {1}, Text: {2}", GhsPrecautionaryStatementId, LanguageCode,
                Translation);
        }
    }
}
