namespace ChemicalSubstancesInventory.Model.Entities
{
    using ChemicalSubstancesInventory.Model.Entities.Configuration;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ChemicalInventoryContext : DbContext, IChemicalInventoryContext
    {
        // Your context has been configured to use a 'ChemicalInventoryModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ChemicalSubstancesInventory.Model.Entities.ChemicalInventoryModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ChemicalInventoryModel' 
        // connection string in the application configuration file.
        public ChemicalInventoryContext()
            : base("name=ChemicalInventoryContext")
        {
            Database.SetInitializer<ChemicalInventoryContext>(new ChemicalInventoryDbInitializer());
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public IDbSet<GhsHazardStatement> GhsHazardStatements { get; set; }
        public IDbSet<Substance> Substances { get; set; }
        public IDbSet<GhsHazardPictogram> GhsHazardPictograms { get; set; }
        public IDbSet<GhsPrecautionaryStatement> GhsPrecautionaryStatements { get; set; }
        public IDbSet<GhsHazardStatementTranslation> GhsHazardStatementTranslations { get; set; }
        public IDbSet<GhsPrecautionaryStatementTranslation> GhsPrecautionaryStatementTranslations { get; set; }
        public IDbSet<HazardClass> HazardClasses { get; set; }
        public IDbSet<HazardClassCategory> HazardClassCategories { get; set; }
        public IDbSet<GhsHazardPictogramTranslation> GhsHazardPictogramTranslations { get; set; }

    }
}