﻿using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.DataImporters.Interfaces
{
    interface IHazardClassCategoriesDataSource
    {
        List<HazardClassCategory> GetHazardClassCategories();
    }
}
