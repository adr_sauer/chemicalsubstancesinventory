﻿using ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources;
using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.MvcAppTests.Infrastructure.MockDbContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.MvcAppTests.Model.DataImporters
{
    [TestClass]
    public class SubstancesAnnexVIExcelDataSourceTests
    {
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void Should_ThrowFileNotFoundExceptionException_IfImportFileDoesNotExist()
        {
            //Arrange
            string filename = "AnnexVINonExistentFile.xlsx";
            var dataSource = new SubstancesAnnexVIExcelDataSource(filename, null);

            if (File.Exists(filename))
            {
                Assert.Inconclusive("File that should not exist - exists.");
            }

            //Act
            var result = dataSource.GetSubstances();

            //Assert - Expects exception
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        [DeploymentItem(@"Model/DataImporters/AnnexVIMalformedFile1.xlsx", @"Model/DataImporters")]
        public void Should_ThrowIOExceptionException_IfImportFileIsMalformed1()
        {
            //Arrange
            string filename = "./Model/DataImporters/AnnexVIMalformedFile1.xlsx";

            var dataSource = new SubstancesAnnexVIExcelDataSource(filename, null);

            string a = Directory.GetCurrentDirectory();
            Console.WriteLine(a);

            if (!File.Exists(filename))
            {
                Assert.Inconclusive("Test file does not exist.");
            }

            //Act
            var result = dataSource.GetSubstances();

            //Assert - Expects exception
        }

        [TestMethod]
        [DeploymentItem(@"Model/DataImporters/AnnexVIEmptyFile.xlsx", @"Model/DataImporters")]
        public void Should_ReturnEmptyList_ForEmptyFile()
        {
            //Arrange
            string filename = "./Model/DataImporters/AnnexVIEmptyFile.xlsx";

            IChemicalInventoryContext dbContext = new MockChemicalInventoryContext()
            {

            };

            var dataSource = new SubstancesAnnexVIExcelDataSource(filename, dbContext);

            if (!File.Exists(filename))
            {
                Assert.Inconclusive("Test file does not exist.");
            }

            //Act
            var result = dataSource.GetSubstances();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        [DeploymentItem(@"Model/DataImporters/AnnexVITestfile1.xlsx", @"Model/DataImporters")]
        public void Should_Import1Substance_ForTestfile1()
        {
            //Arrange
            string filename = "./Model/DataImporters/AnnexVITestfile1.xlsx";

            IChemicalInventoryContext dbContext = new MockChemicalInventoryContext();

            var dataSource = new SubstancesAnnexVIExcelDataSource(filename, dbContext);

            if (!File.Exists(filename))
            {
                Assert.Inconclusive("Test file does not exist.");
            }

            //Act
            var result = dataSource.GetSubstances();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod]
        [DeploymentItem(@"Model/DataImporters/AnnexVITestfile1.xlsx", @"Model/DataImporters")]
        public void Should_ImportRightCAS_ForTestfile1()
        {
            //Arrange
            string filename = "./Model/DataImporters/AnnexVITestfile1.xlsx";

            IChemicalInventoryContext dbContext = new MockChemicalInventoryContext();

            var dataSource = new SubstancesAnnexVIExcelDataSource(filename, dbContext);

            if (!File.Exists(filename))
            {
                Assert.Inconclusive("Test file does not exist.");
            }

            //Act
            var result = dataSource.GetSubstances();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("1333-74-0", result[0].CasNumber);
        }

        [TestMethod]
        [DeploymentItem(@"Model/DataImporters/AnnexVITestfile1.xlsx", @"Model/DataImporters")]
        public void Should_ImportRightEC_ForTestfile1()
        {
            //Arrange
            string filename = "./Model/DataImporters/AnnexVITestfile1.xlsx";

            IChemicalInventoryContext dbContext = new MockChemicalInventoryContext();

            var dataSource = new SubstancesAnnexVIExcelDataSource(filename, dbContext);

            if (!File.Exists(filename))
            {
                Assert.Inconclusive("Test file does not exist.");
            }

            //Act
            var result = dataSource.GetSubstances();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("215-605-7", result[0].EcNumber);
        }

        [TestMethod]
        [DeploymentItem(@"Model/DataImporters/AnnexVITestfile1.xlsx", @"Model/DataImporters")]
        public void Should_ImportRightName_ForTestfile1()
        {
            //Arrange
            string filename = "./Model/DataImporters/AnnexVITestfile1.xlsx";
            

            IChemicalInventoryContext dbContext = new MockChemicalInventoryContext();

            var dataSource = new SubstancesAnnexVIExcelDataSource(filename, dbContext);

            if (!File.Exists(filename))
            {
                Assert.Inconclusive("Test file does not exist.");
            }

            //Act
            var result = dataSource.GetSubstances();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("hydrogen", result[0].IupacName);
        }

        [TestMethod]
        [DeploymentItem(@"Model/DataImporters/AnnexVITestfile1.xlsx", @"Model/DataImporters")]
        public void Should_ImportRightHazardClasses_ForTestfile1()
        {
            //Arrange
            string filename = "./Model/DataImporters/AnnexVITestfile1.xlsx";

            IChemicalInventoryContext dbContext = new MockChemicalInventoryContext()
            {
                HazardClassCategories = {
                    new HazardClassCategory { HazardCategoryCode = "Flam. Gas 1" },
                    new HazardClassCategory { HazardCategoryCode = "Press. Gas" }
                }
            };

            var dataSource = new SubstancesAnnexVIExcelDataSource(filename, dbContext);

            if (!File.Exists(filename))
            {
                Assert.Inconclusive("Test file does not exist.");
            }

            //Act
            var result = dataSource.GetSubstances();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.IsNotNull(result[0].HazardClassCategories);
            Assert.AreEqual(2, result[0].HazardClassCategories.Count);
            Assert.IsNotNull(result[0].HazardClassCategories.Where(q => q.HazardCategoryCode == "Flam. Gas 1").FirstOrDefault());
            Assert.IsNotNull(result[0].HazardClassCategories.Where(q => q.HazardCategoryCode == "Press. Gas").FirstOrDefault());
        }

        [TestMethod]
        [DeploymentItem(@"Model/DataImporters/AnnexVITestfile1.xlsx", @"Model/DataImporters")]
        public void Should_ImportRightHazardStatements_ForTestfile1()
        {
            //Arrange
            string filename = "./Model/DataImporters/AnnexVITestfile1.xlsx";
            string hazardStatementCode1 = "H220";

            IChemicalInventoryContext dbContext = new MockChemicalInventoryContext()
            {
                GhsHazardStatements = { new GhsHazardStatement { GhsHazardStatementCode = hazardStatementCode1 } }
            };

            var dataSource = new SubstancesAnnexVIExcelDataSource(filename, dbContext);

            if (!File.Exists(filename))
            {
                Assert.Inconclusive("Test file does not exist.");
            }

            //Act
            var result = dataSource.GetSubstances();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.IsNotNull(result[0].GhsHazardStatements);
            Assert.AreEqual(1, result[0].GhsHazardStatements.Count);
            Assert.IsNotNull(result[0].GhsHazardStatements.Where(q => q.GhsHazardStatementCode == hazardStatementCode1).FirstOrDefault());
        }
    }
}
