﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class GhsHazardPictogramTranslation
    {
        [Key]
        [Column(Order = 1)]
        public int GhsHazardPictogramId { get; set; }

        [Key]
        [Column(Order = 2)]
        [MaxLength(5)]
        public string LanguageCode { get; set; }

        public virtual GhsHazardPictogram GhsHazardPictogram { get; set; }

        [Required]
        public string Translation { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0}, Language: {1}, Text: {2}", GhsHazardPictogramId, LanguageCode,
                Translation);
        }
    }
}
