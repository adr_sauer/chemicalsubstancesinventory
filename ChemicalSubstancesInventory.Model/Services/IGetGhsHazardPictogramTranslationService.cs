﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;

namespace ChemicalSubstancesInventory.Model.Services
{
    public interface IGetGhsHazardPictogramTranslationService : ICsiServiceWithObjectResult<string>
    {
        int GhsHazardPictogramId { get; set; }
        string LanguageCode { get; set; }
    }
}
