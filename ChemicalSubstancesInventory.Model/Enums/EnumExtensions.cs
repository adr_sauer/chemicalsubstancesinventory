﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Enums
{

    /// <summary>
    /// Contains extension methods for enums.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Returns GHS code string for enum value
        /// </summary>
        /// <param name="enumValue">Enum value for which to return GHS code</param>
        /// <returns>Ghs code for enum value of null if no code exists</returns>
        public static string GetGhsCode(this Enum enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            GhsCodeAttribute[] attributes =
                (GhsCodeAttribute[])fi.GetCustomAttributes(typeof(GhsCodeAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].GhsCode;
            }                
            else
            {
                return null;
            }                
        }
        public static string GetDescription(this Enum enumValue)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return enumValue.ToString();
        }
    }
}
