﻿using ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChemicalSubstancesInventory.MvcAppTests.Model.DataImporters
{
    [TestClass]
    public class GhsHazardPictogramsHardcodeDataSourceTests
    {
        [TestMethod]
        public void Should_ReturnHardcodedTestPictogramsList()
        {
            var dataSource = new GhsHazardPictogramsHardcodeDataSource();

            var result = dataSource.GetGhsHazardPictograms();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count == 9);
        }
    }
}
