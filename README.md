﻿# README #

### Chemical Substances Inventory ###

* Quick summary

This ASP .NET MVC application serves as a demonstration of my skills as a C# developer.

* Version 0.*

### Technologies used ###

* .NET Framework 	
	
* Entity Framework

	Application model was created using Code First approach.

	Files where used: ChemicalSubstancesInventory.Model project.

* ASP .NET MVC

* Unit testing

	Unit tests implemented using Microsoft Unit Test Framework

	Files where used: ChemicalSubstancesInventory.MvcApp.Tests project.
	


### Contact ###

* Adrian Sauer

My LinkedIn profile: https://pl.linkedin.com/in/adrian-sauer-692b86138