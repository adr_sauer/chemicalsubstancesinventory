﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Enums;
using ChemicalSubstancesInventory.Model.Services;
using ChemicalSubstancesInventory.Model.Services.Concrete;
using ChemicalSubstancesInventory.Model.Services.Dto;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;
using ChemicalSubstancesInventory.MvcAppTests.Infrastructure.MockDbContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.MvcAppTests.Model.Services
{
    [TestClass]
    public class GetSubstancePictogramsChoiceListServiceTests
    {
        [TestMethod]
        public void Should_ReturnNull_ForInvalidSubstanceId()
        {
            //Arrange
            IGetSubstancePictogramsChoiceListService service = null;

            Substance substance1 = null;
            GhsHazardPictogram pictogram1 = null;

            try
            {
                pictogram1 = new GhsHazardPictogram() { GhsHazardPictogramId = 1, GhsPictogramCode = "GHS01", SvgData = "SVG1", GhsPictogramEnglishName = "NAME1" };

                substance1 = new Substance()
                {
                    SubstanceId = 1,
                    CasNumber = "CAS1",
                    EcNumber = "EC1",
                    IupacName = "name1",
                    GhsSignalWordEnumValue = GhsSignalWordEnum.Warning,
                    HazardClassCategories = new HashSet<HazardClassCategory>(),
                    GhsHazardPictograms = new HashSet<GhsHazardPictogram>(),
                    GhsHazardStatements = new HashSet<GhsHazardStatement>(),
                    GhsPrecautionaryStatements = new HashSet<GhsPrecautionaryStatement>()
                };

                substance1.GhsHazardPictograms.Add(pictogram1);


                IChemicalInventoryContext dbContext = new MockChemicalInventoryContext()
                {
                    GhsHazardPictograms =
                {
                   pictogram1
                }
                    ,
                    Substances = { substance1 }
                };

                service = new GetSubstancePictogramsChoiceListService(dbContext);

            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            service.SubstanceId = 3;
            CsiServiceCollectionResult<PictogramChoiceListElementDto> result = service.Execute();

            //Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.AnyErrorOccured);
            Assert.IsNotNull(result.Errors);
            Assert.IsTrue(result.Errors.Count > 0);

        }

        [TestMethod]
        public void Should_ReturnValidPictogramChoiceListElementDto_ForValidArguments()
        {
            IGetSubstancePictogramsChoiceListService service = null;

            Substance substance1 = null;
            GhsHazardPictogram pictogram1 = null;
            GhsHazardPictogram pictogram2 = null;
            GhsHazardPictogram pictogram3 = null;
            GhsHazardPictogram pictogram4 = null;


            try
            {
                pictogram1 = new GhsHazardPictogram() { GhsHazardPictogramId = 1, GhsPictogramCode = "GHS01", SvgData = "SVG1", GhsPictogramEnglishName = "NAME1" };
                pictogram2 = new GhsHazardPictogram() { GhsHazardPictogramId = 2, GhsPictogramCode = "GHS02", SvgData = "SVG2", GhsPictogramEnglishName = "NAME2" };
                pictogram3 = new GhsHazardPictogram() { GhsHazardPictogramId = 3, GhsPictogramCode = "GHS03", SvgData = "SVG3", GhsPictogramEnglishName = "NAME3" };
                pictogram4 = new GhsHazardPictogram() { GhsHazardPictogramId = 4, GhsPictogramCode = "GHS04", SvgData = "SVG4", GhsPictogramEnglishName = "NAME4" };

                substance1 = new Substance()
                {
                    SubstanceId = 1,
                    CasNumber = "CAS1",
                    EcNumber = "EC1",
                    IupacName = "name1",
                    GhsSignalWordEnumValue = GhsSignalWordEnum.Warning,
                    HazardClassCategories = new HashSet<HazardClassCategory>(),
                    GhsHazardPictograms = new HashSet<GhsHazardPictogram>(),
                    GhsHazardStatements = new HashSet<GhsHazardStatement>(),
                    GhsPrecautionaryStatements = new HashSet<GhsPrecautionaryStatement>()
                };

                substance1.GhsHazardPictograms.Add(pictogram1);
                substance1.GhsHazardPictograms.Add(pictogram3);

                Substance substance2 = new Substance()
                {
                    SubstanceId = 2,
                    CasNumber = "CAS2",
                    EcNumber = "EC2",
                    IupacName = "name2",
                    GhsSignalWordEnumValue = GhsSignalWordEnum.None,
                    HazardClassCategories = new HashSet<HazardClassCategory>(),
                    GhsHazardPictograms = new HashSet<GhsHazardPictogram>(),
                    GhsHazardStatements = new HashSet<GhsHazardStatement>(),
                    GhsPrecautionaryStatements = new HashSet<GhsPrecautionaryStatement>()
                };


                IChemicalInventoryContext dbContext = new MockChemicalInventoryContext()
                {
                    GhsHazardPictograms =
                {
                   pictogram1,pictogram2,pictogram3,pictogram4
                }
                    ,
                    Substances = { substance1, substance2 }
                };

                service = new GetSubstancePictogramsChoiceListService(dbContext);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            service.SubstanceId = 1;
            CsiServiceCollectionResult<PictogramChoiceListElementDto> serviceResult = service.Execute();

            //Assert
            Assert.IsNotNull(serviceResult);
            Assert.IsFalse(serviceResult.AnyErrorOccured);
            Assert.IsNotNull(serviceResult.Result);

            var result = serviceResult.Result;

            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);

            var chosenPictogramsDto = result.Where(q => q.IsChosen == true).ToList();
            Assert.AreEqual(2, chosenPictogramsDto.Count);
            Assert.AreEqual(2, chosenPictogramsDto.Where(q => q.Pictogram != null).Count());

            var pictogram1Dto = chosenPictogramsDto.Where(q => q.Pictogram.GhsHazardPictogramId == pictogram1.GhsHazardPictogramId).Select(q => q.Pictogram).Single();
            var pictogram3Dto = chosenPictogramsDto.Where(q => q.Pictogram.GhsHazardPictogramId == pictogram3.GhsHazardPictogramId).Select(q => q.Pictogram).Single();
            Assert.AreEqual(pictogram1.GhsPictogramCode, pictogram1Dto.GhsPictogramCode);
            Assert.AreEqual(pictogram3.GhsPictogramCode, pictogram3Dto.GhsPictogramCode);


        }
    }
}
