﻿using ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace ChemicalSubstancesInventory.MvcAppTests.Model.DataImporters
{
    [TestClass]
    public class HazardClassCategoriesHardcodeDataSourceTests
    {
        [TestMethod]
        public void Should_ReturnHardcodedTestHazardClassesList()
        {
            var dataSource = new HazardClassCategoriesHardcodeDataSource();

            var result = dataSource.GetHazardClassCategories();

            Assert.IsNotNull(result);    
            Assert.IsTrue(result.Count == 76);
            var classes = result.Select(q => q.HazardClass).Distinct().ToList();
            Assert.IsNotNull(classes);
            Assert.IsTrue(classes.Count == 28);
        }
    }
}
