﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.MvcApp.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ChemicalSubstancesInventory.MvcApp.Infrastructure;
using ChemicalSubstancesInventory.Model.Enums;
using ChemicalSubstancesInventory.MvcApp.Models.Enums;
using ChemicalSubstancesInventory.Model.Services;
using System.Threading;
using ChemicalSubstancesInventory.Model.Services.Dto;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;

namespace ChemicalSubstancesInventory.MvcApp.Controllers
{
    public partial class SubstanceController : Controller
    {
        IChemicalInventoryContext _dbContext;
        public SubstanceController(IChemicalInventoryContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: Substance
        public virtual RedirectToRouteResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual ViewResult List(string message = null)
        {
            var viewModel = new SubstanceListViewModel();


            viewModel.SubstanceList = _dbContext.Substances.Include(q => q.GhsHazardPictograms).Include(q => q.GhsHazardStatements)
                .Include(q => q.HazardClassCategories).ToList();


            viewModel.Message = message;

            return View(viewModel);
        }

        [HttpPost]
        public virtual ActionResult Delete(int? id)
        {
            string message = null;

            if (id.HasValue)
            {
                Substance substance = _dbContext.Substances.Find(id);
                if (substance != null)
                {
                    _dbContext.Substances.Remove(substance);
                    _dbContext.SaveChanges();
                    message = "Substance successfully deleted.";
                }
                else
                {
                    message = "Substance not found in database.";
                }
            }
            else
            {
                message = "Error. No substance id provided.";
            }

            return RedirectToAction("List", new { message });
        }

        [HttpPost]
        public virtual ActionResult Edit(SubstanceEditViewModel viewModel = null)
        {
            bool success = false;

            if (viewModel != null && ModelState.IsValid)
            {
                Substance substance = _dbContext.Substances.Find(viewModel.SubstanceId);
                if (substance != null)
                {
                    substance.CasNumber = viewModel.CasNumber;
                    substance.EcNumber = viewModel.EcNumber;
                    substance.IupacName = viewModel.IupacName;
                    substance.GhsSignalWordEnumValue = viewModel.GhsSignalWordEnumValue;

                    var chosenPictogramIds = viewModel.Pictograms.Where(q => q.Chosen).Select(q => q.GhsHazardPictogramId).ToList();

                    var chosenPictograms = _dbContext.GhsHazardPictograms.Where(q => chosenPictogramIds.Contains(q.GhsHazardPictogramId)).ToList();

                    substance.GhsHazardPictograms.Clear();

                    chosenPictograms.ForEach(q => substance.GhsHazardPictograms.Add(q));

                    _dbContext.SaveChanges();
                    success = true;
                }
            }

            if (success)
            {
                return RedirectToAction("List", new { message = "Substance successfully saved." });
            }
            else
            {
                if (viewModel != null && viewModel.SubstanceId >= 0 && viewModel.Pictograms != null)
                {
                    return View(viewModel);
                }
                else
                {
                    return RedirectToAction("List", new { message = "Error during saving substance." });
                }
            }
        }

        public virtual ActionResult Edit(int? id)
        {
            ActionResult result = null;
            CsiServiceObjectResult<SubstanceEditorDto> serviceResult = null;
            //Substance substance = null;
            SubstanceEditViewModel viewModel;
            //List<GhsHazardPictogram> allPictogramsList = new List<GhsHazardPictogram>();

            IGetSubstanceEditorDtoService service = DependencyResolver.Current.GetService<IGetSubstanceEditorDtoService>();

            if (ModelState.IsValid && id.HasValue)
            {
                service.SubstanceId = id.Value;
                service.LanguageCode = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
                //serviceResult = service.GetSubstanceEditorDto(id.Value, Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName);
                serviceResult = service.Execute();

                if (!serviceResult.AnyErrorOccured)
                {
                    IMapper mapper = new CsiMapperConfiguration().CreateMapper();

                    viewModel = mapper.Map<SubstanceEditorDto, SubstanceEditViewModel>(serviceResult.Result);

                    result = View(viewModel);
                }


                //substance = _dbContext.Substances.Include(q => q.GhsHazardPictograms).Where(q => q.SubstanceId == id).FirstOrDefault();
                //allPictogramsList = _dbContext.GhsHazardPictograms.ToList();

                //if (substance != null)
                //{


                //    viewModel = mapper.Map<Substance, SubstanceEditViewModel>(substance);

                //    foreach (var pictogram in allPictogramsList)
                //    {
                //        var p = mapper.Map<GhsHazardPictogram, SubstanceEditViewModelPictogram>(pictogram);
                //        viewModel.Pictograms.Add(p);
                //    }

                //    foreach (var pictogram in substance.GhsHazardPictograms)
                //    {
                //        foreach (var p in viewModel.Pictograms.Where(q => q.GhsHazardPictogramId == pictogram.GhsHazardPictogramId))
                //        {
                //            p.Chosen = true;
                //        }
                //    }

                //    result = View(viewModel);
                //}
            }
            else
            {
                result = RedirectToAction("List", new { errorMessage = "error" });
            }

            if (serviceResult == null || serviceResult.AnyErrorOccured)
            {
                result = RedirectToAction("List", new { errorMessage = "error" });
            }

            return result;

        }
    }
}