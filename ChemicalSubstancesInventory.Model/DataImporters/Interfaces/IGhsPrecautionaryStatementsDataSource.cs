﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChemicalSubstancesInventory.Model.Entities;

namespace ChemicalSubstancesInventory.Model.DataImporters.Interfaces
{
    public interface IGhsPrecautionaryStatementsDataSource
    {
        List<GhsPrecautionaryStatement> GetGhsPrecautionaryStatements();
    }
}
