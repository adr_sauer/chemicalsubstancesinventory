﻿using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemicalSubstancesInventory.MvcApp.Models.ViewModels
{
    public class SubstanceListViewModel
    {
        public List<Substance> SubstanceList { get; set; }
        public string Message { get; set; }
    }
}