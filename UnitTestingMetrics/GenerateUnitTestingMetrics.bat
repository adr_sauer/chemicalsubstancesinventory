REM Create a 'GeneratedReports' folder if it does not exist
if not exist "%~dp0GeneratedReports" mkdir "%~dp0GeneratedReports"

REM Remove any previous test execution files to prevent issues overwriting
IF EXIST "%~dp0TestExecutionFiles" RMDIR "%~dp0TestExecutionFiles" /S /Q

REM Create a 'TestExecutionFiles' folder if it does not exist
if not exist "%~dp0TestExecutionFiles" mkdir "%~dp0TestExecutionFiles"
 
REM Remove any previous test execution files to prevent issues overwriting
IF EXIST "%~dp0ChemicalSubstancesInventoryTestResult.trx" del "%~dp0ChemicalSubstancesInventoryTestResult.trx%"

REM Run the tests against the targeted output
call :RunOpenCoverUnitTestMetrics

PAUSE
 
REM Generate the report output based on the test results
if %errorlevel% equ 0 (
 call :RunReportGeneratorOutput
)

REM Launch the report
if %errorlevel% equ 0 (
 call :RunLaunchReport
)

exit /b %errorlevel%

:RunOpenCoverUnitTestMetrics
"%~dp0\..\packages\OpenCover.4.6.519\tools\OpenCover.Console.exe" ^
-register:user ^
-target:"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\mstest.exe" ^
-targetargs:"/testcontainer:\"%~dp0..\ChemicalSubstancesInventory.MvcAppTests\bin\Debug\ChemicalSubstancesInventory.MvcAppTests.dll\" /resultsfile:\"%~dp0TestExecutionFiles\ChemicalSubstancesInventoryTestResult.trx\"" ^
-filter:"+[ChemicalSubstancesInventory*]* -[ChemicalSubstancesInventory.MvcAppTests]* -[*]ChemicalSubstancesInventory.MvcApp.Controllers.T4MVC* -[ChemicalSubstancesInventory.MvcApp]Links* -[ChemicalSubstancesInventory.MvcApp]MVC -[ChemicalSubstancesInventory.MvcApp]T4MVC*" ^
-mergebyhash ^
-skipautoprops ^
-output:"%~dp0\GeneratedReports\ChemicalSubstancesInventoryReport.xml"
exit /b %errorlevel%

:RunReportGeneratorOutput
"%~dp0\..\packages\ReportGenerator.3.1.2\tools\ReportGenerator.exe" ^
-reports:"%~dp0\GeneratedReports\ChemicalSubstancesInventoryReport.xml" ^
-targetdir:"%~dp0\GeneratedReports\ReportGenerator Output"
exit /b %errorlevel%
 
:RunLaunchReport
start "report" "%~dp0\GeneratedReports\ReportGenerator Output\index.htm"
exit /b %errorlevel%
