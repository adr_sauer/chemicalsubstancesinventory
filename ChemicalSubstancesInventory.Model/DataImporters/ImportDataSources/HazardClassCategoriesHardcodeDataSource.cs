﻿using ChemicalSubstancesInventory.Model.DataImporters.Interfaces;
using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources
{
    public class HazardClassCategoriesHardcodeDataSource : IHazardClassCategoriesDataSource
    {
        public List<HazardClassCategory> GetHazardClassCategories()
        {
            HazardClass hazardClass;
            HazardClassCategory hazardClassCategory;
            List<HazardClassCategory> result = new List<HazardClassCategory>();

            hazardClass = new HazardClass()
            {
                EnglishName= "Explosive"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Unst. Expl.",
                HazardClass = hazardClass                
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Expl. 1.1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Expl. 1.2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Expl. 1.3",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Expl. 1.4",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Expl. 1.5",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Expl. 1.6",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Flammable gas"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Flam. Gas 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Flam. Gas 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Flammable aerosol"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Flam. Aerosol 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Flam. Aerosol 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Oxidising gas"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Ox. Gas 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Gases under pressure"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Press. Gas",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Flammable liquid"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Flam. Liq. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Flam. Liq. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Flam. Liq. 3",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Flammable solid"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Flam. Sol. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Flam. Sol. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Self-reactive substance or mixture"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Self-react. A",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Self-react. B",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Self-react. CD",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Self-react. EF",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Self-react. G",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Pyrophoric liquid"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Pyr. Liq. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Pyrophoric solid"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Pyr. Sol. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Self-heating substance or mixture"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Self-heat. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Self-heat. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Substance or mixture which in contact with water emits flammable gas"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Water-react. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Water-react. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Water-react. 3",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Oxidising liquid"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Ox. Liq. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Ox. Liq. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Ox. Liq. 3",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Oxidising solid"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Ox. Sol. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Ox. Sol. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Ox. Sol. 3",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Organic peroxide"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Org. Perox. A",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Org. Perox. B",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Org. Perox. CD",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Org. Perox. EF",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Org. Perox. G",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Substance or mixture corrosive to metals"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Met. Corr. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Acute toxicity"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Acute Tox. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Acute Tox. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Acute Tox. 3",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Acute Tox. 4",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Skin corrosion/irritation"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Skin Corr. 1A",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Skin Corr. 1B",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Skin Corr. 1C",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Skin Irrit. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Serious eye damage/eye irritation "
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Eye Dam. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Eye Irrit. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Respiratory/skin sensitization"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Resp. Sens. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Skin Sens. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Germ cell mutagenicity"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Muta. 1A",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Muta. 1B",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Muta. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Carcinogenicity"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Carc. 1A",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Carc. 1B",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Carc. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Reproductive toxicity"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Repr. 1A",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Repr. 1B",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Repr. 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Lact.",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Specific target organ toxicity — single exposure"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "STOT SE 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "STOT SE 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "STOT SE 3",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Specific target organ toxicity — repeated exposure"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "STOT RE 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "STOT RE 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Aspiration hazard"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Asp. Tox. 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Hazardous to the aquatic environment"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Aquatic Acute 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Aquatic Chronic 1",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Aquatic Chronic 2",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Aquatic Chronic 3",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Aquatic Chronic 4",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            hazardClass = new HazardClass()
            {
                EnglishName = "Hazardous for the ozone layer"
            };

            hazardClassCategory = new HazardClassCategory()
            {
                HazardCategoryCode = "Ozone",
                HazardClass = hazardClass
            };
            result.Add(hazardClassCategory);

            return result;
        }
    }
}
