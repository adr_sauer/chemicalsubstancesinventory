﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChemicalSubstancesInventory.Model.Validators;

namespace ChemicalSubstancesInventory.MvcAppTests.Model.Validators
{
    /*
     A CASRN is separated by hyphens into three parts, the first consisting from two up to seven digits, the second consisting of two digits, 
     and the third consisting of a single digit serving as a check digit. 
     This current format gives CAS a maximum capacity of 1,000,000,000 unique identifiers.

    The check digit is found by taking the last digit times 1, the previous digit times 2, the previous digit times 3 etc., 
    adding all these up and computing the sum modulo 10. For example, the CAS number of water is 7732-18-5: 
    the checksum 5 is calculated as (8×1 + 1×2 + 2×3 + 3×4 + 7×5 + 7×6) = 105; 105 mod 10 = 5.
        */

    [TestClass]
    public class CASValidatorTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Should_ThrowArgumentNullException_IfCASStringIsNull()
        {
            //Arrange
            string casString = null;
            var validator = new CASValidator();

            //Act
            bool result = validator.IsValidCASNumber(casString);

            //Assert - Expects exception
        }

        [TestMethod]
        public void Should_ReturnFalse_IfCASIsEmpty()
        {
            //Arrange
            string casString = "";
            var validator = new CASValidator();

            //Act
            bool result = validator.IsValidCASNumber(casString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnFalse_IfCASContainsALetter()
        {
            //Arrange
            string casString = "7732-1a-5";
            var validator = new CASValidator();

            //Act
            bool result = validator.IsValidCASNumber(casString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnFalse_IfCASContainsOnlyOneHyphen()
        {
            //Arrange
            string casString = "773218-5";
            var validator = new CASValidator();

            //Act
            bool result = validator.IsValidCASNumber(casString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnFalse_IfCASContainsThreeHyphens()
        {
            //Arrange
            string casString = "77-32-18-5";
            var validator = new CASValidator();

            //Act
            bool result = validator.IsValidCASNumber(casString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnFalse_IfCASMoreThanOneDigitInLastSection()
        {
            //Arrange
            string casString = "7732-18-52";
            var validator = new CASValidator();

            //Act
            bool result = validator.IsValidCASNumber(casString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnFalse_IfCASCheckDigitIsWrong()
        {
            //Arrange
            string casString = "7732-18-3";
            var validator = new CASValidator();

            //Act
            bool result = validator.IsValidCASNumber(casString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnTrue_IfCASIsValid()
        {
            //Arrange
            string casString = "7732-18-5";
            var validator = new CASValidator();

            //Act
            bool result = validator.IsValidCASNumber(casString);

            //Assert
            Assert.IsTrue(result);
        }
    }
}
