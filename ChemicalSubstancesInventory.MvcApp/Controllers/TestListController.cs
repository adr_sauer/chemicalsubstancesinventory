﻿using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChemicalSubstancesInventory.MvcApp.Controllers
{
    public partial class TestListController : Controller
    {
        // GET: TestList
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult HazardStatements()
        {
            List<GhsHazardStatement> list = null;
            using (DbContext context = new ChemicalInventoryContext())
            {
                list = context.Set<GhsHazardStatement>().ToList();
            }

            return View(list);
        }

        public virtual ActionResult HazardClasses()
        {
            List<HazardClass> list = null;
            using (DbContext context = new ChemicalInventoryContext())
            {
                list = context.Set<HazardClass>().ToList();

                foreach (var item in list)
                {
                    item.HazardClassCategories = item.HazardClassCategories.ToList();
                }
            }

            return View(list);
        }

        public virtual ActionResult Substances()
        {
            List<Substance> list = null;

            using (DbContext context = new ChemicalInventoryContext())
            {
                list = context.Set<Substance>().Include(q => q.GhsHazardPictograms).Include(q => q.GhsHazardStatements)
                    .Include(q => q.HazardClassCategories).ToList();
            }

            return View(list);
        }

    }
}