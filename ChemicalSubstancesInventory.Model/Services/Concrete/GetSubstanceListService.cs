﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Services.Dto;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;

namespace ChemicalSubstancesInventory.Model.Services.Concrete
{
    public class GetSubstanceListService : IGetSubstanceListService
    {
        public GetSubstanceListService(IChemicalInventoryContext dbContext)
        {
            _dbContext = dbContext;
        }

        private IChemicalInventoryContext _dbContext;

        public CsiServiceCollectionResult<SubstanceListElementDto> Execute()
        {
            var errorList = new List<CsiServiceError>();
            IQueryable<SubstanceListElementDto> resultQueryable = null;


            throw new NotImplementedException();
        }
    }
}
