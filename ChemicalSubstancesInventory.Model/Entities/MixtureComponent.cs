﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class MixtureComponent
    {
        public int MixtureComponentId { get; set; }

        public int MixtureId { get; set; }

        public virtual Mixture Mixture { get; set; }

        [Range(0,100)]
        public double ContentPercent { get; set; }        

        public int SubstanceId { get; set; }

        public virtual Substance Substance { get; set; }

    }
}
