﻿using ChemicalSubstancesInventory.Model.DataImporters.Interfaces;
using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Enums;
using ChemicalSubstancesInventory.Model.Validators;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources
{
    public class SubstancesAnnexVIExcelDataSource : ISubstancesDataSource
    {
        private string _excelFilePathname;
        //private IHazardClassCategoryRepository _hazardClassCategoryRepository;
        //private IGhsHazardStatementRepository _hazardStatementRepository;
        //private IGhsHazardPictogramRepository _hazardPictogramRepository;
        private List<AnnexVIExcelError> _errorList;
        private IChemicalInventoryContext _dbContext;

        public SubstancesAnnexVIExcelDataSource(string excelFilePathname, IChemicalInventoryContext dbContext)
        {
            _excelFilePathname = excelFilePathname;
            //_hazardClassCategoryRepository = hazardClassCategoryRepository;
            //_hazardStatementRepository = hazardStatementRepository;
            //_hazardPictogramRepository = hazardPictogramRepository;
            _errorList = new List<AnnexVIExcelError>();
            _dbContext = dbContext;
        }

        public class AnnexVIExcelError
        {
            public string IndexNo { get; set; }
            public string ErrorMessage { get; set; }

            public override string ToString()
            {
                return string.Format("Index: {0} Error: {1}", IndexNo, ErrorMessage);
            }
        }

        private class AnnexVIExcelEntry
        {
            public string IndexNo { get; set; }
            public string InternationChemicalIdentification { get; set; }
            public string ECNumber { get; set; }
            public string CASNumber { get; set; }
            public string ClassificationHazardCategories { get; set; }
            public string ClassificationHazardStatements { get; set; }
            public string LabellingPictogramAndSignalWord { get; set; }
            public string LabellingHazardStatements { get; set; }
            public string LabellingSupplementaryHazardStatements { get; set; }
            public string SpecificConcentrationFactors { get; set; }
            public string Notes { get; set; }
            public string ATPUpdatedOrInserted { get; set; }

        }

        public List<AnnexVIExcelError> ErrorList
        {
            get
            {
                return _errorList;
            }
        }

        public List<Substance> GetSubstances()
        {
            if (!File.Exists(_excelFilePathname))
            {
                throw new FileNotFoundException();
            }

            var result = new List<Substance>();
            var excelEntries = new List<AnnexVIExcelEntry>(); ;

            try
            {
                ReadExcelFile(excelEntries);


                _errorList.Clear();

                var hazardStatementDictionary = _dbContext.GhsHazardStatements.ToDictionary(q => q.GhsHazardStatementCode);
                var hazardPictogramsDictionary = _dbContext.GhsHazardPictograms.ToDictionary(q => q.GhsPictogramCode);
                var signalWordDictionary = BuildGhsSignalWordEnumDictionary();
                var hazardCategoryDictionary = _dbContext.HazardClassCategories.ToDictionary(q => q.HazardCategoryCode);

                foreach (var entry in excelEntries)
                {
                    var error = false;
                    var substance = new Substance();

                    substance.IupacName = entry.InternationChemicalIdentification;

                    if (new ECNumberValidator().IsValidECNumber(entry.ECNumber))
                    {
                        substance.EcNumber = entry.ECNumber;
                    }
                    else
                    {
                        error = true;
                        _errorList.Add(new AnnexVIExcelError() { IndexNo = entry.IndexNo, ErrorMessage = "EC number parsing error." });
                    }

                    if (new CASValidator().IsValidCASNumber(entry.CASNumber))
                    {
                        substance.CasNumber = entry.CASNumber;
                    }
                    else
                    {
                        error = true;
                        _errorList.Add(new AnnexVIExcelError() { IndexNo = entry.IndexNo, ErrorMessage = "CAS number parsing error." });
                    }

                    string[] hazardClassesCodes = entry.ClassificationHazardCategories.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string hazardClassesCode in hazardClassesCodes)
                    {
                        //TODO Make changes to correctly parse values like 'Acute Tox. 4 *'. Maybe regex?

                        //Temporary solution. Might not always work
                        string code = hazardClassesCode;
                        code = code.Replace('*', ' ');
                        code = code.Trim();

                        if (hazardCategoryDictionary.ContainsKey(code))
                        {
                            HazardClassCategory hazardClass = hazardCategoryDictionary[code];
                            substance.HazardClassCategories = substance.HazardClassCategories ?? new List<HazardClassCategory>();
                            substance.HazardClassCategories.Add(hazardClass);
                        }

                        //HazardClassCategory hazardClass = _hazardClassCategoryRepository.GetByCode(hazardClassesCode);
                        //if (hazardClass != null)
                        //{
                        //    substance.HazardClassCategories = substance.HazardClassCategories ?? new List<HazardClassCategory>();
                        //    substance.HazardClassCategories.Add(hazardClass);
                        //}
                    }

                    string[] hazardStatementCodes = entry.LabellingHazardStatements.Split(new[] { '\n', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string hazardStatementCode in hazardStatementCodes)
                    {
                        if (hazardStatementDictionary.ContainsKey(hazardStatementCode))
                        {
                            GhsHazardStatement hazardStatement = hazardStatementDictionary[hazardStatementCode];
                            substance.GhsHazardStatements = substance.GhsHazardStatements ?? new List<GhsHazardStatement>();
                            substance.GhsHazardStatements.Add(hazardStatement);
                        }
                    }

                    string[] hazardPicrogramOrSignalWordCodes = entry.LabellingPictogramAndSignalWord.Split(new[] { '\n', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string hazardPicrogramOrSignalWordCode in hazardPicrogramOrSignalWordCodes)
                    {
                        if (hazardPictogramsDictionary.ContainsKey(hazardPicrogramOrSignalWordCode))
                        {
                            GhsHazardPictogram hazardPictogram = hazardPictogramsDictionary[hazardPicrogramOrSignalWordCode];
                            substance.GhsHazardPictograms = substance.GhsHazardPictograms ?? new List<GhsHazardPictogram>();
                            substance.GhsHazardPictograms.Add(hazardPictogram);
                        }
                        else if (signalWordDictionary.ContainsKey(hazardPicrogramOrSignalWordCode))
                        {
                            substance.GhsSignalWordEnumValue = signalWordDictionary[hazardPicrogramOrSignalWordCode];
                        }
                    }

                    if (!error)
                    {
                        result.Add(substance);
                    }
                }
            }
            catch (Exception e)
            {
                throw new IOException("File malformed.", e);
            }


            return result;
        }

        private void ReadExcelFile(List<AnnexVIExcelEntry> excelEntries)
        {
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(_excelFilePathname, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;

                IEnumerable<Sheet> sheetcollection = spreadsheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();

                string relationshipId = sheetcollection.First().Id.Value;

                WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(relationshipId);

                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                Row[] rowcollection = sheetData.Descendants<Row>().ToArray();

                if (rowcollection.Length > 0)
                {
                    for (int rowIndex = 2; rowIndex < rowcollection.Count(); rowIndex++)
                    {
                        Cell[] rowCells = rowcollection[rowIndex].Descendants<Cell>().ToArray();
                        if (rowCells.Length >= 12)
                        {
                            var entry = new AnnexVIExcelEntry();
                            entry.IndexNo = GetValueOfCell(spreadsheetDocument, rowCells[0]);
                            if (!string.IsNullOrWhiteSpace(entry.IndexNo))
                            {
                                entry.InternationChemicalIdentification = GetValueOfCell(spreadsheetDocument, rowCells[1]);
                                entry.ECNumber = GetValueOfCell(spreadsheetDocument, rowCells[2]);
                                entry.CASNumber = GetValueOfCell(spreadsheetDocument, rowCells[3]);
                                entry.ClassificationHazardCategories = GetValueOfCell(spreadsheetDocument, rowCells[4]);
                                entry.ClassificationHazardStatements = GetValueOfCell(spreadsheetDocument, rowCells[5]);
                                entry.LabellingPictogramAndSignalWord = GetValueOfCell(spreadsheetDocument, rowCells[6]);
                                entry.LabellingHazardStatements = GetValueOfCell(spreadsheetDocument, rowCells[7]);
                                entry.LabellingSupplementaryHazardStatements = GetValueOfCell(spreadsheetDocument, rowCells[8]);
                                entry.SpecificConcentrationFactors = GetValueOfCell(spreadsheetDocument, rowCells[9]);
                                entry.Notes = GetValueOfCell(spreadsheetDocument, rowCells[10]);
                                entry.ATPUpdatedOrInserted = GetValueOfCell(spreadsheetDocument, rowCells[11]);
                                excelEntries.Add(entry);
                            }
                        }
                    }
                }
            }
        }


        /// <summary> 
        ///  Pobranie wartości komórki  
        /// </summary> 
        /// <param name="spreadsheetdocument">SpreadSheet Document Object</param> 
        /// <param name="cell">Cell Object</param> 
        /// <returns>The Value in Cell</returns> 
        private static string GetValueOfCell(SpreadsheetDocument spreadsheetdocument, Cell cell)
        {
            // Pobranie wartości w komórce 
            SharedStringTablePart sharedString = spreadsheetdocument.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
            {
                return string.Empty;
            }


            string cellValue = cell.CellValue.InnerText;

            // Warunek: typ danych komórki to SharedString 
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return sharedString.SharedStringTable.ChildElements[int.Parse(cellValue)].InnerText;
            }
            else
            {
                return cellValue;
            }
        }

        private Dictionary<string, GhsSignalWordEnum> BuildGhsSignalWordEnumDictionary()
        {
            var result = new Dictionary<string, GhsSignalWordEnum>();

            foreach (GhsSignalWordEnum signalWord in Enum.GetValues(typeof(GhsSignalWordEnum)))
            {
                string code = signalWord.GetGhsCode();
                if (code != null)
                {
                    result.Add(code, signalWord);
                }
            }

            return result;
        }
    }
}
