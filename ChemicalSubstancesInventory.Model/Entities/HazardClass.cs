﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class HazardClass
    {
        public int HazardClassId { get; set; }

        [MaxLength(200)]
        [Required]
        public string EnglishName { get; set; }

        public virtual ICollection<HazardClassCategory> HazardClassCategories { get; set; }
    }
}
