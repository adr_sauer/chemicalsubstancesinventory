﻿using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemicalSubstancesInventory.MvcApp.Models.ViewModels
{
    public class TestSubstanceListViewModel
    {
        public class SubstanceView
        {
            public SubstanceView(Substance entity)
            {
                Name = entity.IupacName;
                CASNumber = entity.CasNumber;
                ECNumber = entity.EcNumber;
                SignalWord = entity.GhsSignalWordEnumValue.ToString();

                PictogramSvgs = new List<string>();

                foreach (var p in entity.GhsHazardPictograms)
                {
                    PictogramSvgs.Add(p.SvgData);
                }

                HazardStatements = new List<string>();
                foreach (var h in entity.GhsHazardStatements)
                {
                    HazardStatements.Add(h.GhsHazardStatementCode + " " + h.EnglishText);
                }

            }


            public string Name { get; set; }
            public string CASNumber { get; set; }
            public string ECNumber { get; set; }
            public string SignalWord { get; set; }

            public List<string> PictogramSvgs { get; set; }
            public List<string> HazardStatements { get; set; }

        }

        public List<SubstanceView> Substances { get; set; }


        public TestSubstanceListViewModel(IList<Substance> entityList)
        {
            Substances = new List<SubstanceView>();

            foreach (var entity in entityList)
            {
                Substances.Add(new SubstanceView(entity));
            }
        }
    }
}