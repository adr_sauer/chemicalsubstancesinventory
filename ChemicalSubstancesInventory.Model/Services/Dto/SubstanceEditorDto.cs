﻿using ChemicalSubstancesInventory.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Dto
{
    public class SubstanceEditorDto
    {
        public int SubstanceId { get; set; }
        public string IupacName { get; set; }
        public string CasNumber { get; set; }
        public string EcNumber { get; set; }
        public GhsSignalWordEnum GhsSignalWordEnumValue { get; set; }
        public IList<SubstanceEditorPictogramDto> PictogamsChoiceList { get; set; }
    }

    public class SubstanceEditorPictogramDto
    {
        public int GhsHazardPictogramId { get; set; }
        public bool Chosen { get; set; }
        public string GhsPictogramCode { get; set; }
        public string TranslatedDescription { get; set; }
    }
}
