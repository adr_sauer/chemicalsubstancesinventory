﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public interface ICsiServiceWithObjectResult<T> : ICsiService<CsiServiceObjectResult<T>> where T : class
    {

    }
}
