﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Enums;
using ChemicalSubstancesInventory.Model.Services;
using ChemicalSubstancesInventory.Model.Services.Concrete;
using ChemicalSubstancesInventory.Model.Services.Dto;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;
using ChemicalSubstancesInventory.MvcAppTests.Infrastructure.MockDbContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.MvcAppTests.Model.Services
{
    [TestClass]
    public class GetSubstanceEditorDtoServiceTests
    {
        [TestMethod]
        public void Should_ReturnRightEditorDto_ForSubstance()
        {

            Substance substance1 = null;
            GhsHazardPictogram pictogram1 = null;
            GhsHazardPictogram pictogram2 = null;
            GhsHazardPictogram pictogram3 = null;
            GhsHazardPictogram pictogram4 = null;
            //GhsHazardPictogramTranslation translation1 = null;
            //GhsHazardPictogramTranslation translation2 = null;
            //GhsHazardPictogramTranslation translation3 = null;
            //GhsHazardPictogramTranslation translation4 = null;
            IGetSubstanceEditorDtoService service = null;
            //IGetSubstancePictogramsChoiceListService choiceListService = null;
            //Arrange
            try
            {
                pictogram1 = new GhsHazardPictogram() { GhsHazardPictogramId = 1, GhsPictogramCode = "GHS01", SvgData = "SVG1", GhsPictogramEnglishName = "NAME1" };
                pictogram2 = new GhsHazardPictogram() { GhsHazardPictogramId = 2, GhsPictogramCode = "GHS02", SvgData = "SVG2", GhsPictogramEnglishName = "NAME2" };
                pictogram3 = new GhsHazardPictogram() { GhsHazardPictogramId = 3, GhsPictogramCode = "GHS03", SvgData = "SVG3", GhsPictogramEnglishName = "NAME3" };
                pictogram4 = new GhsHazardPictogram() { GhsHazardPictogramId = 4, GhsPictogramCode = "GHS04", SvgData = "SVG4", GhsPictogramEnglishName = "NAME4" };

                //translation1 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 1, LanguageCode = "pl", Translation = "TRANS1" };

                substance1 = new Substance()
                {
                    SubstanceId = 1,
                    CasNumber = "CAS1",
                    EcNumber = "EC1",
                    IupacName = "name1",
                    GhsSignalWordEnumValue = GhsSignalWordEnum.Warning,
                    HazardClassCategories = new HashSet<HazardClassCategory>(),
                    GhsHazardPictograms = new HashSet<GhsHazardPictogram>(),
                    GhsHazardStatements = new HashSet<GhsHazardStatement>(),
                    GhsPrecautionaryStatements = new HashSet<GhsPrecautionaryStatement>()
                };

                Substance substance2 = new Substance()
                {
                    SubstanceId = 2,
                    CasNumber = "CAS2",
                    EcNumber = "EC2",
                    IupacName = "name2",
                    GhsSignalWordEnumValue = GhsSignalWordEnum.None,
                    HazardClassCategories = new HashSet<HazardClassCategory>(),
                    GhsHazardPictograms = new HashSet<GhsHazardPictogram>(),
                    GhsHazardStatements = new HashSet<GhsHazardStatement>(),
                    GhsPrecautionaryStatements = new HashSet<GhsPrecautionaryStatement>()
                };

                //substance1.GhsHazardPictograms.Add(pictogram1);
                //substance1.GhsHazardPictograms.Add(pictogram3);


                IChemicalInventoryContext dbContext = new MockChemicalInventoryContext()
                {
                    GhsHazardPictograms =
                {
                   pictogram1,pictogram2,pictogram3,pictogram4
                }
                    ,
                    Substances = { substance1, substance2 }
                };

                var choicelist = new List<PictogramChoiceListElementDto>()
                {
                    new PictogramChoiceListElementDto(){IsChosen=true,Pictogram=pictogram1},
                    new PictogramChoiceListElementDto(){IsChosen=false,Pictogram=pictogram2},
                    new PictogramChoiceListElementDto(){IsChosen=true,Pictogram=pictogram3},
                    new PictogramChoiceListElementDto(){IsChosen=false,Pictogram=pictogram4},
                };


                var translationServiceMock = new Mock<IGetGhsHazardPictogramTranslationService>();


                CsiServiceObjectResult<string> serviceResult1 = new CsiServiceObjectResult<string>(new List<CsiServiceError>(), "translation");

                translationServiceMock.Setup(q => q.Execute()).Returns(serviceResult1);

                var choiceListServiceMock = new Mock<IGetSubstancePictogramsChoiceListService>();

                CsiServiceCollectionResult<PictogramChoiceListElementDto> serviceResult2 = new CsiServiceCollectionResult<PictogramChoiceListElementDto>(new List<CsiServiceError>(), choicelist.AsQueryable());

                choiceListServiceMock.Setup(q => q.Execute()).Returns(serviceResult2);
                //choiceListService = choiceListServiceMock.Object;

                service = new GetSubstanceEditorDtoService(dbContext, choiceListServiceMock.Object, translationServiceMock.Object);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            service.SubstanceId = 1;
            service.LanguageCode = "pl";
            var serviceResult= service.Execute();


            //Assert
            Assert.IsNotNull(serviceResult);
            Assert.IsFalse(serviceResult.AnyErrorOccured);
            Assert.IsNotNull(serviceResult.Result); 

            SubstanceEditorDto result = serviceResult.Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(substance1.CasNumber, result.CasNumber);
            Assert.AreEqual(substance1.EcNumber, result.EcNumber);
            Assert.AreEqual(substance1.GhsSignalWordEnumValue, result.GhsSignalWordEnumValue);
            Assert.AreEqual(substance1.IupacName, result.IupacName);
            Assert.AreEqual(substance1.SubstanceId, result.SubstanceId);
            Assert.IsNotNull(result.PictogamsChoiceList);

            foreach (var pictogram in result.PictogamsChoiceList)
            {
                Assert.IsNotNull(pictogram.TranslatedDescription);
            }
        }
    }
}
