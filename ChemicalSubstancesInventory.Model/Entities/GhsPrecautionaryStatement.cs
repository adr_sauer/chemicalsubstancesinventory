﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class GhsPrecautionaryStatement
    {
        public int GhsPrecautionaryStatementId { get; set; }

        [Required]
        [MaxLength(30)]
        public string GhsPrecautionaryStatementCode { get; set; }

        [Required]
        [MaxLength(300)]
        public string EnglishText { get; set; }

        public bool NeedsSupplement { get; set; }

        public virtual ICollection<Substance> Substances { get; set; }

        public virtual ICollection<GhsPrecautionaryStatementTranslation> GhsPrecautionaryStatementTranslations { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0}, Code: {1}, Text: {2}",GhsPrecautionaryStatementId, GhsPrecautionaryStatementCode,
                EnglishText);
        }
    }
}
