﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Services;
using ChemicalSubstancesInventory.Model.Services.Concrete;
using ChemicalSubstancesInventory.MvcAppTests.Infrastructure.MockDbContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.MvcAppTests.Model.Services
{
    [TestClass]
    public class GetGhsHazardPictogramTranslationServiceTests
    {
        [TestMethod]
        public void Should_ReturnValidTranslation_ForValidArguments()
        {
            //Arrange
            GhsHazardPictogram pictogram1 = null;
            GhsHazardPictogram pictogram2 = null;
            GhsHazardPictogramTranslation translation1 = null;
            GhsHazardPictogramTranslation translation2 = null;
            GhsHazardPictogramTranslation translation3 = null;
            GhsHazardPictogramTranslation translation4 = null;

            IGetGhsHazardPictogramTranslationService service = null;

            try
            {
                pictogram1 = new GhsHazardPictogram() { GhsHazardPictogramId = 1, GhsPictogramCode = "GHS01", SvgData = "SVG1", GhsPictogramEnglishName = "NAME1", GhsHazardPictogramTranslations = new HashSet<GhsHazardPictogramTranslation>() };
                pictogram2 = new GhsHazardPictogram() { GhsHazardPictogramId = 2, GhsPictogramCode = "GHS02", SvgData = "SVG2", GhsPictogramEnglishName = "NAME2", GhsHazardPictogramTranslations = new HashSet<GhsHazardPictogramTranslation>() };

                translation1 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 1, LanguageCode = "en", Translation = "tr1" };
                translation2 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 1, LanguageCode = "pl", Translation = "tr2" };
                translation3 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 2, LanguageCode = "en", Translation = "tr3" };
                translation4 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 2, LanguageCode = "pl", Translation = "tr4" };

                pictogram1.GhsHazardPictogramTranslations.Add(translation1);
                pictogram1.GhsHazardPictogramTranslations.Add(translation2);
                pictogram2.GhsHazardPictogramTranslations.Add(translation3);
                pictogram2.GhsHazardPictogramTranslations.Add(translation4);

                IChemicalInventoryContext dbContext = new MockChemicalInventoryContext()
                {
                    GhsHazardPictograms =
                {
                   pictogram1,pictogram2
                }
                    ,
                    GhsHazardPictogramTranslations = { translation1, translation2
                    ,translation3, translation4}
                };

                service = new GetGhsHazardPictogramTranslationService(dbContext);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            service.GhsHazardPictogramId = 2;
            service.LanguageCode = "en";
            var serviceResult = service.Execute();

            //Assert
            Assert.IsNotNull(serviceResult);
            Assert.IsFalse(serviceResult.AnyErrorOccured);
            var result = serviceResult.Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(translation3.Translation, result);
        }

        [TestMethod]
        public void Should_ReturnNull_ForInvalidGhsHazardPictogramId()
        {
            //Arrange
            GhsHazardPictogram pictogram1 = null;
            GhsHazardPictogram pictogram2 = null;
            GhsHazardPictogramTranslation translation1 = null;
            GhsHazardPictogramTranslation translation2 = null;
            GhsHazardPictogramTranslation translation3 = null;
            GhsHazardPictogramTranslation translation4 = null;

            IGetGhsHazardPictogramTranslationService service = null;

            try
            {
                pictogram1 = new GhsHazardPictogram() { GhsHazardPictogramId = 1, GhsPictogramCode = "GHS01", SvgData = "SVG1", GhsPictogramEnglishName = "NAME1", GhsHazardPictogramTranslations = new HashSet<GhsHazardPictogramTranslation>() };
                pictogram2 = new GhsHazardPictogram() { GhsHazardPictogramId = 2, GhsPictogramCode = "GHS02", SvgData = "SVG2", GhsPictogramEnglishName = "NAME2", GhsHazardPictogramTranslations = new HashSet<GhsHazardPictogramTranslation>() };

                translation1 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 1, LanguageCode = "en", Translation = "tr1" };
                translation2 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 1, LanguageCode = "pl", Translation = "tr2" };
                translation3 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 2, LanguageCode = "en", Translation = "tr3" };
                translation4 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 2, LanguageCode = "pl", Translation = "tr4" };

                pictogram1.GhsHazardPictogramTranslations.Add(translation1);
                pictogram1.GhsHazardPictogramTranslations.Add(translation2);
                pictogram2.GhsHazardPictogramTranslations.Add(translation3);
                pictogram2.GhsHazardPictogramTranslations.Add(translation4);

                IChemicalInventoryContext dbContext = new MockChemicalInventoryContext()
                {
                    GhsHazardPictograms =
                {
                   pictogram1,pictogram2
                }
                    ,
                    GhsHazardPictogramTranslations = { translation1, translation2
                    ,translation3, translation4}
                };

                service = new GetGhsHazardPictogramTranslationService(dbContext);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            //var result = service.GetGhsHazardPictogramTranslation(3, "en");
            service.GhsHazardPictogramId = 3;
            service.LanguageCode = "en";
            var serviceResult = service.Execute();

            //Assert
            Assert.IsNotNull(serviceResult);
            Assert.IsTrue(serviceResult.AnyErrorOccured);
            Assert.IsNull(serviceResult.Result);
        }


        [TestMethod]
        public void Should_ReturnNull_ForInvalidLanguageCode()
        {
            //Arrange
            GhsHazardPictogram pictogram1 = null;
            GhsHazardPictogram pictogram2 = null;
            GhsHazardPictogramTranslation translation1 = null;
            GhsHazardPictogramTranslation translation2 = null;
            GhsHazardPictogramTranslation translation3 = null;
            GhsHazardPictogramTranslation translation4 = null;

            IGetGhsHazardPictogramTranslationService service = null;

            try
            {
                pictogram1 = new GhsHazardPictogram() { GhsHazardPictogramId = 1, GhsPictogramCode = "GHS01", SvgData = "SVG1", GhsPictogramEnglishName = "NAME1", GhsHazardPictogramTranslations = new HashSet<GhsHazardPictogramTranslation>() };
                pictogram2 = new GhsHazardPictogram() { GhsHazardPictogramId = 2, GhsPictogramCode = "GHS02", SvgData = "SVG2", GhsPictogramEnglishName = "NAME2", GhsHazardPictogramTranslations = new HashSet<GhsHazardPictogramTranslation>() };

                translation1 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 1, LanguageCode = "en", Translation = "tr1" };
                translation2 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 1, LanguageCode = "pl", Translation = "tr2" };
                translation3 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 2, LanguageCode = "en", Translation = "tr3" };
                translation4 = new GhsHazardPictogramTranslation() { GhsHazardPictogramId = 2, LanguageCode = "pl", Translation = "tr4" };

                pictogram1.GhsHazardPictogramTranslations.Add(translation1);
                pictogram1.GhsHazardPictogramTranslations.Add(translation2);
                pictogram2.GhsHazardPictogramTranslations.Add(translation3);
                pictogram2.GhsHazardPictogramTranslations.Add(translation4);

                IChemicalInventoryContext dbContext = new MockChemicalInventoryContext()
                {
                    GhsHazardPictograms =
                {
                   pictogram1,pictogram2
                }
                    ,
                    GhsHazardPictogramTranslations = { translation1, translation2
                    ,translation3, translation4}
                };

                service = new GetGhsHazardPictogramTranslationService(dbContext);
            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            //var result = service.GetGhsHazardPictogramTranslation(2, "e3");
            service.GhsHazardPictogramId = 2;
            service.LanguageCode = "e3";
            var serviceResult = service.Execute();

            //Assert
            Assert.IsNotNull(serviceResult);
            Assert.IsTrue(serviceResult.AnyErrorOccured);
            Assert.IsNull(serviceResult.Result);
        }
    }
}
