﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using ChemicalSubstancesInventory.Model.DataImporters.Interfaces;
using ChemicalSubstancesInventory.Model.Entities;

namespace ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources
{
    public class GhsPrecautionaryStatementsKeminacoXmlDataSource : IGhsPrecautionaryStatementsDataSource
    {   
        private string _filename;
        private const string _englishLangcode = "EN";

        public GhsPrecautionaryStatementsKeminacoXmlDataSource(string filename)
        {
            _filename = filename;
        }

        public List<GhsPrecautionaryStatement> GetGhsPrecautionaryStatements()
        {
            var dictionary = GetPrecautionaryStatementsDictionary();
            if (dictionary != null)
            {
                var result = (from q in dictionary select q.Value).ToList();
                return result;
            }
            else
            {
                return null;
            }
        }

        public Dictionary<string, GhsPrecautionaryStatement> GetPrecautionaryStatementsDictionary()
        {
            Dictionary<string, GhsPrecautionaryStatement> result = null;
            try
            {
                XDocument document = XDocument.Load(_filename);
                result = new Dictionary<string, GhsPrecautionaryStatement>();

                var englishPhraseNodes = document.Root.XPathSelectElements(string.Format(@"/Phrases/Phrase[@langcode='{0}' and @type='Precautionary statement']", _englishLangcode));
                if (englishPhraseNodes != null)
                {
                    foreach (var node in englishPhraseNodes)
                    {
                        GhsPrecautionaryStatement hazardStatement = new GhsPrecautionaryStatement();
                        hazardStatement.GhsPrecautionaryStatementCode = node.Attribute("code").Value;
                        hazardStatement.EnglishText = node.Value;
                        hazardStatement.GhsPrecautionaryStatementTranslations = new List<GhsPrecautionaryStatementTranslation>();
                        result.Add(hazardStatement.GhsPrecautionaryStatementCode, hazardStatement);
                    }
                }

                var translationNodes = document.Root.XPathSelectElements(string.Format(@"/Phrases/Phrase[@type='Precautionary statement']", _englishLangcode));
                if (translationNodes != null)
                {
                    foreach (var node in translationNodes)
                    {
                        GhsPrecautionaryStatementTranslation translation = new GhsPrecautionaryStatementTranslation();
                        translation.LanguageCode = (string)node.Attribute("langcode");
                        translation.Translation = node.Value;

                        GhsPrecautionaryStatement statement = result[node.Attribute("code").Value];
                        translation.GhsPrecautionaryStatement = statement;
                        statement.GhsPrecautionaryStatementTranslations.Add(translation);

                    }
                }
            }
            catch (Exception)
            {
                result = null;
            }

            return result;
        }
    }
}
