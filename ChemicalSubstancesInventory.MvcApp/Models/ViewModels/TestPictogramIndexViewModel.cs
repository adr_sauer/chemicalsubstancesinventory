﻿using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemicalSubstancesInventory.MvcApp.Models.ViewModels
{
    public class TestPictogramIndexViewModel
    {
        public TestPictogramIndexViewModel()
        {
            Pictograms = new List<Pictogram>();
        }

        public TestPictogramIndexViewModel(IList<GhsHazardPictogram> entityList)
        {
            Pictograms = new List<Pictogram>();

            foreach (var entity in entityList)
            {
                Pictograms.Add(new Pictogram(entity));
            }
        }

        public class Pictogram
        {
            public Pictogram(GhsHazardPictogram entity)
            {
                SvgData = entity.SvgData;
                Description = entity.GhsPictogramEnglishName;
                Code = entity.GhsPictogramCode;
            }

            public string SvgData { get; set; }

            public HtmlString Svg
            {
                get
                {
                    return new HtmlString(SvgData);
                }
            }

            public string Description { get; set; }

            public string Code { get; set; }
        }

        public List<Pictogram> Pictograms { get; set; }
    }
}