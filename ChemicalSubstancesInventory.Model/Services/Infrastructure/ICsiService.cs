﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public interface ICsiService<ResultType> where ResultType : CsiServiceResult
    {
        ResultType Execute();
    }
}
