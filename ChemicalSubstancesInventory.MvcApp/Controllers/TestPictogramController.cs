﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.MvcApp.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChemicalSubstancesInventory.MvcApp.Controllers
{
    public partial class TestPictogramController : Controller
    {
        public virtual ActionResult Index()
        {
            List<GhsHazardPictogram> list = new List<GhsHazardPictogram>();
            var viewModel = new TestPictogramIndexViewModel();
            using (var ctx = new ChemicalInventoryContext())
            {
                list = ctx.GhsHazardPictograms.ToList();
                viewModel = new TestPictogramIndexViewModel(list);
            }

            return View(viewModel);
        }
    }
}