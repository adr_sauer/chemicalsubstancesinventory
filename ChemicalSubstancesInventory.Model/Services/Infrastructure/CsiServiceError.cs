﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public class CsiServiceError
    {
        public CsiServiceError(string message)
        {
            Message = message;
        }

        public virtual string Message { get; private set; }

        public override string ToString()
        {
            return Message ?? string.Empty;
        }
    }
}
