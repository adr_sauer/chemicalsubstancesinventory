﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Services.Dto;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;

namespace ChemicalSubstancesInventory.Model.Services.Concrete
{
    public class GetSubstanceEditorDtoService : IGetSubstanceEditorDtoService
    {
        private IChemicalInventoryContext _dbContext;
        private IGetSubstancePictogramsChoiceListService _getSubstancePictogramsChoiceListService;
        private IGetGhsHazardPictogramTranslationService _getGhsHazardPictogramTranslationService;

        public int SubstanceId { get; set; }
        public string LanguageCode { get; set; }

        public GetSubstanceEditorDtoService(IChemicalInventoryContext dbContext, IGetSubstancePictogramsChoiceListService getSubstancePictogramsChoiceListService,
            IGetGhsHazardPictogramTranslationService getGhsHazardPictogramTranslationService)
        {
            _dbContext = dbContext;
            _getSubstancePictogramsChoiceListService = getSubstancePictogramsChoiceListService;
            _getGhsHazardPictogramTranslationService = getGhsHazardPictogramTranslationService;
        }

        public CsiServiceObjectResult<SubstanceEditorDto> Execute()
        {
            var errorList = new List<CsiServiceError>();
            SubstanceEditorDto result = null;

            try
            {
                Substance substance = _dbContext.Substances.Find(SubstanceId);

                if (substance != null)
                {
                    result = new SubstanceEditorDto
                    {
                        CasNumber = substance.CasNumber,
                        EcNumber = substance.EcNumber,
                        GhsSignalWordEnumValue = substance.GhsSignalWordEnumValue,
                        IupacName = substance.IupacName,
                        SubstanceId = substance.SubstanceId,
                        PictogamsChoiceList = new List<SubstanceEditorPictogramDto>()
                    };

                    _getSubstancePictogramsChoiceListService.SubstanceId = SubstanceId;
                    var getSubstancePictogramsChoiceListServiceResult = _getSubstancePictogramsChoiceListService.Execute();
                    if (!getSubstancePictogramsChoiceListServiceResult.AnyErrorOccured)
                    {
                        result.PictogamsChoiceList = getSubstancePictogramsChoiceListServiceResult.Result.
                            Select(q => new SubstanceEditorPictogramDto() { 
                                GhsHazardPictogramId=q.Pictogram.GhsHazardPictogramId,
                                GhsPictogramCode  = q.Pictogram.GhsPictogramCode,
                                Chosen= q.IsChosen
                            }).ToList();


                        foreach(var choiceListElement in result.PictogamsChoiceList)
                        {
                            //choiceListElement.Chosen = substance.GhsHazardPictograms.Select(q => q.GhsHazardPictogramId).Contains(choiceListElement.GhsHazardPictogramId);
                            _getGhsHazardPictogramTranslationService.GhsHazardPictogramId = choiceListElement.GhsHazardPictogramId;
                            _getGhsHazardPictogramTranslationService.LanguageCode = LanguageCode;
                            var translationServiceResult = _getGhsHazardPictogramTranslationService.Execute();

                            if (!translationServiceResult.AnyErrorOccured)
                            {
                                choiceListElement.TranslatedDescription = translationServiceResult.Result;
                            }
                            else
                            {
                                choiceListElement.TranslatedDescription = string.Empty;
                                errorList.Add(new CsiServiceError($"Ghs hazard pictogram with ID: {choiceListElement.GhsHazardPictogramId} translation not found."));
                            }
                        }                        
                    }
                    else
                    {
                        errorList.AddRange(getSubstancePictogramsChoiceListServiceResult.Errors);
                        result = null;
                    }
                }
                else
                {
                    errorList.Add(new CsiServiceError($"Substance with ID: {SubstanceId} not found."));
                }
            }
            catch (Exception ex)
            {
                errorList.Add(new CsiServiceExceptionError(ex));
            }

            return new CsiServiceObjectResult<SubstanceEditorDto>(errorList, result);
        }

        //public SubstanceEditorDto GetSubstanceEditorDto(int substanceId, string languageCode)
        //{
        //    SubstanceEditorDto result = null;

        //    try
        //    {
        //        Substance substance = _dbContext.Substances.Find(substanceId);

        //        if (substance != null)
        //        {
        //            result = new SubstanceEditorDto
        //            {
        //                CasNumber = substance.CasNumber,
        //                EcNumber = substance.EcNumber,
        //                GhsSignalWordEnumValue = substance.GhsSignalWordEnumValue,
        //                IupacName = substance.IupacName,
        //                SubstanceId = substance.SubstanceId,
        //                PictogamsChoiceList = new List<SubstanceEditorPictogramDto>()
        //            };

        //            _getSubstancePictogramsChoiceListService.SubstanceId = substanceId;

        //            var getSubstancePictogramsChoiceListServiceResult = _getSubstancePictogramsChoiceListService.Execute();
        //            if (!getSubstancePictogramsChoiceListServiceResult.AnyErrorOccured)
        //            {
        //                foreach (var pictogram in getSubstancePictogramsChoiceListServiceResult.QueryableResult)
        //                {
        //                    var tmp = new SubstanceEditorPictogramDto()
        //                    {
        //                        GhsHazardPictogramId = pictogram.Pictogram.GhsHazardPictogramId,
        //                        GhsPictogramCode = pictogram.Pictogram.GhsPictogramCode,
        //                        Chosen = substance.GhsHazardPictograms.Contains(pictogram.Pictogram)
        //                    };

        //                    _getGhsHazardPictogramTranslationService.GhsHazardPictogramId = pictogram.Pictogram.GhsHazardPictogramId;
        //                    _getGhsHazardPictogramTranslationService.LanguageCode = languageCode;
        //                    var translationServiceResult = _getGhsHazardPictogramTranslationService.Execute();

        //                    if (!translationServiceResult.AnyErrorOccured)
        //                    {
        //                        tmp.TranslatedDescription = translationServiceResult.Result;
        //                    }
        //                    else
        //                    {
        //                        tmp.TranslatedDescription = pictogram.Pictogram.GhsPictogramEnglishName;
        //                    }

        //                    //string translation = _getGhsHazardPictogramTranslationService.GetGhsHazardPictogramTranslation(pictogram.Pictogram.GhsHazardPictogramId, languageCode);

        //                    //if(translation != null)
        //                    //{
        //                    //    tmp.TranslatedDescription = translation;
        //                    //}
        //                    //else
        //                    //{
        //                    //    tmp.TranslatedDescription = pictogram.Pictogram.GhsPictogramEnglishName;
        //                    //}

        //                    result.PictogamsChoiceList.Add(tmp);
        //                }
        //            }
        //            else
        //            {
        //                //TODO Add errors to error list
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        result = null;
        //    }

        //    return result;
        //}
    }
}
