﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public class CsiServiceNullableResult<ResultType> : CsiServiceResult where ResultType : struct
    {
        public CsiServiceNullableResult(ICollection<CsiServiceError> errors, Nullable<ResultType> result = null) : base(errors)
        {
            Result = result;
        }

        public Nullable<ResultType> Result { get; private set; }
    }
}
