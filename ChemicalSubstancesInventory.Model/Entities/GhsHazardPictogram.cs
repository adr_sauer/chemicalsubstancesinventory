﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class GhsHazardPictogram
    {
        public int GhsHazardPictogramId { get; set; }

        [Required]
        [MaxLength(10)]
        [StringLength(10, MinimumLength = 1)]
        public string GhsPictogramCode { get; set; }

        [Required]
        [MaxLength(300)]
        [StringLength(300, MinimumLength = 1)]
        public string GhsPictogramEnglishName { get; set; }

        [Required]
        public string SvgData { get; set; }

        public virtual ICollection<Substance> Substances { get; set; }

        public virtual ICollection<GhsHazardPictogramTranslation> GhsHazardPictogramTranslations { get; set; }
                
        public override string ToString()
        {
            return string.Format("Id: {0} Pictogram: {1} {2}", GhsHazardPictogramId, GhsPictogramCode, GhsPictogramEnglishName);
        }
    }
}
