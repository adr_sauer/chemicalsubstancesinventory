﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ChemicalSubstancesInventory.Model.Validators
{
    /// <summary>
    /// Provides validation of EC (European Community number) numbers.
    /// </summary>
    public class ECNumberValidator
    {
        private const string _ecRegexPattern = @"^\d{3}-\d{3}-\d$";
        private const int _ecSegmentsCount = 3;
        private Regex _ecRegex;

        /// <summary>
        /// Initializes a new instance of <see cref="ECNumberValidator"/> class.
        /// </summary>
        public ECNumberValidator()
        {
            _ecRegex = new Regex(_ecRegexPattern, RegexOptions.CultureInvariant);
        }

        /// <summary>
        /// Checks if a given string is a valid EC number.
        /// </summary>
        /// <param name="ecNumber">EC number string to validate.</param>
        /// <returns><b>True</b> if argument is valid EC number. <b>False</b> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown if argument is null.</exception>
        public bool IsValidECNumber(string ecNumber)
        {
            bool result = true;
            if (ecNumber == null)
            {
                throw new ArgumentNullException("casNumber");
            }

            if (_ecRegex.IsMatch(ecNumber))
            {
                string[] ecNumberSegments = ecNumber.Split('-');
                int lastDigit = Int32.Parse(ecNumberSegments[ecNumberSegments.Length - 1]);
                result = (lastDigit == CalculateCheckDigit(ecNumberSegments));
            }
            else
            {
                result = false;
            }

            return result;
        }

        private int CalculateCheckDigit(string[] ecNumberSegments)
        {
            int[] ecDigits = new int[ecNumberSegments[0].Length + ecNumberSegments[1].Length];

            int i = 0;
            foreach (char c in ecNumberSegments[0])
            {
                ecDigits[i] = Int32.Parse(c.ToString());
                i++;
            }

            foreach (char c in ecNumberSegments[1])
            {
                ecDigits[i] = Int32.Parse(c.ToString());
                i++;
            }

            int sum = 0;
            for (int j = 0, times = 1; j < ecDigits.Length; j++, times++)
            {
                sum += ecDigits[j] * times;
            }

            return sum % 11;
        }
    }
}
