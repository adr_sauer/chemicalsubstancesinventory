﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Concrete
{
    public class GetGhsHazardPictogramTranslationService : IGetGhsHazardPictogramTranslationService
    {
        private IChemicalInventoryContext _dbContext;

        public GetGhsHazardPictogramTranslationService(IChemicalInventoryContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int GhsHazardPictogramId { get; set; }

        public string LanguageCode { get; set; }

        public CsiServiceObjectResult<string> Execute()
        {
            var errorsList = new List<CsiServiceError>();
            string translationString = null;

            try
            {
                var translation = _dbContext.GhsHazardPictogramTranslations.Find(GhsHazardPictogramId, LanguageCode);

                if (translation != null)
                {
                    translationString = translation.Translation;
                }
                else
                {
                    errorsList.Add(new CsiServiceError("Error. Translation not found."));
                }
            }
            catch(Exception ex)
            {
                errorsList.Add(new CsiServiceExceptionError(ex));
                translationString = null;
            }

            return new CsiServiceObjectResult<string>(errorsList, translationString);
        }

        //public string GetGhsHazardPictogramTranslation(int ghsHazardPictogramId, string languageCode)
        //{
        //    var translation = _dbContext.GhsHazardPictogramTranslations.Find(ghsHazardPictogramId, languageCode);

        //    if (translation != null)
        //    {
        //        return translation.Translation;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}
    }
}
