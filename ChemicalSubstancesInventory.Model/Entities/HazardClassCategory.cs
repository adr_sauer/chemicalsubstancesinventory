﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class HazardClassCategory
    {
        public int HazardClassCategoryId { get; set; }

        public int HazardClassId { get; set; }

        public virtual HazardClass HazardClass { get; set; }        

        [Required]
        [MaxLength(50)]
        public string HazardCategoryCode { get; set; }

        public ICollection<Substance> Substances { get; set; }
    }
}
