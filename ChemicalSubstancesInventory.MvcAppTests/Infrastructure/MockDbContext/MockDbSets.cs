﻿using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.MvcAppTests.Infrastructure.MockDbContext
{
    public class MockGhsHazardStatementSet : MockDbSet<GhsHazardStatement>
    {
        public override GhsHazardStatement Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.GhsHazardStatementId == (int)keyValues.Single());
        }
    }

    public class MockSubstanceSet : MockDbSet<Substance>
    {
        public override Substance Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.SubstanceId == (int)keyValues.Single());
        }
    }

    public class MockGhsHazardPictogramSet : MockDbSet<GhsHazardPictogram>
    {
        public override GhsHazardPictogram Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.GhsHazardPictogramId == (int)keyValues.Single());
        }
    }

    public class MockGhsPrecautionaryStatementSet : MockDbSet<GhsPrecautionaryStatement>
    {
        public override GhsPrecautionaryStatement Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.GhsPrecautionaryStatementId == (int)keyValues.Single());
        }
    }

    public class MockGhsHazardStatementTranslationSet : MockDbSet<GhsHazardStatementTranslation>
    {
        public override GhsHazardStatementTranslation Find(params object[] keyValues)
        {
            if(keyValues == null)
            {
                throw new ArgumentNullException();
            }

            if(keyValues.Length != 2)
            {
                throw new InvalidOperationException();
            }

            if(!(keyValues[0] is int))
            {
                throw new InvalidOperationException();
            }

            if (!(keyValues[1] is string))
            {
                throw new InvalidOperationException();
            }

            return this.SingleOrDefault(d => (d.GhsHazardStatementId == (int)keyValues[0] && d.LanguageCode==(string)keyValues[1]));
        }
    }

    public class MockGhsPrecautionaryStatementTranslationSet : MockDbSet<GhsPrecautionaryStatementTranslation>
    {
        public override GhsPrecautionaryStatementTranslation Find(params object[] keyValues)
        {
            if (keyValues == null)
            {
                throw new ArgumentNullException();
            }

            if (keyValues.Length != 2)
            {
                throw new InvalidOperationException();
            }

            if (!(keyValues[0] is int))
            {
                throw new InvalidOperationException();
            }

            if (!(keyValues[1] is string))
            {
                throw new InvalidOperationException();
            }

            return this.SingleOrDefault(d => (d.GhsPrecautionaryStatementId == (int)keyValues[0] && d.LanguageCode == (string)keyValues[1]));
        }
    }

    public class MockHazardClassSet : MockDbSet<HazardClass>
    {
        public override HazardClass Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.HazardClassId == (int)keyValues.Single());
        }
    }

    public class MockHazardClassCategorySet : MockDbSet<HazardClassCategory>
    {
        public override HazardClassCategory Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.HazardClassCategoryId == (int)keyValues.Single());
        }
    }

    public class MockGhsHazardPictogramTranslationSet : MockDbSet<GhsHazardPictogramTranslation>
    {
        public override GhsHazardPictogramTranslation Find(params object[] keyValues)
        {
            if (keyValues == null)
            {
                throw new ArgumentNullException();
            }

            if (keyValues.Length != 2)
            {
                throw new InvalidOperationException();
            }

            if (!(keyValues[0] is int))
            {
                throw new InvalidOperationException();
            }

            if (!(keyValues[1] is string))
            {
                throw new InvalidOperationException();
            }

            return this.SingleOrDefault(d => (d.GhsHazardPictogramId == (int)keyValues[0] && d.LanguageCode == (string)keyValues[1]));
        }
    }
}
