﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Enums
{
    public enum CsiCultureEnum
    {
        en = 0,
        pl = 1
    }
}
