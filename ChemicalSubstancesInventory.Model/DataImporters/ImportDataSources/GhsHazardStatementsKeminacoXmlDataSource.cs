﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using ChemicalSubstancesInventory.Model.DataImporters.Interfaces;
using ChemicalSubstancesInventory.Model.Entities;

namespace ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources
{
    public class GhsHazardStatementsKeminacoXmlDataSource : IGhsHazardStatementsDataSource
    {
        //C:\VisualStudioProjects\ASP\WorkingProjects\ChemicalSubstancesInventory\precautionary_statements.xml

        private string _filename;
        private const string _englishLangcode = "EN";

        public GhsHazardStatementsKeminacoXmlDataSource(string filename)
        {
            _filename = filename;
        }

        public List<GhsHazardStatement> GetHazardStatements()
        {
            var dictionary = GetHazardStatementsDictionary();
            if (dictionary != null)
            {
                var result = (from q in dictionary select q.Value).ToList();
                return result;
            }
            else
            {
                return null;
            }
        }

        public Dictionary<string, GhsHazardStatement> GetHazardStatementsDictionary()
        {
            Dictionary<string, GhsHazardStatement> result = null;
            try
            {
                XDocument document = XDocument.Load(_filename);
                result = new Dictionary<string, GhsHazardStatement>();
                                
                var englishPhraseNodes = document.Root.XPathSelectElements(string.Format(@"/Phrases/Phrase[@langcode='{0}' and @type='Hazard statement']", _englishLangcode));
                if (englishPhraseNodes != null)
                {
                    foreach (var node in englishPhraseNodes)
                    {
                        GhsHazardStatement hazardStatement = new GhsHazardStatement();
                        hazardStatement.GhsHazardStatementCode = node.Attribute("code").Value;
                        hazardStatement.EnglishText = node.Value;
                        hazardStatement.GhsHazardStatementTranslations = new List<GhsHazardStatementTranslation>();
                        result.Add(hazardStatement.GhsHazardStatementCode, hazardStatement);
                    }
                }

                var translationNodes = document.Root.XPathSelectElements(string.Format(@"/Phrases/Phrase[@type='Hazard statement']", _englishLangcode));
                if (translationNodes != null)
                {
                    foreach (var node in translationNodes)
                    {
                        GhsHazardStatementTranslation translation = new GhsHazardStatementTranslation();
                        translation.LanguageCode = (string)node.Attribute("langcode");
                        translation.Translation = node.Value;

                        GhsHazardStatement statement = result[node.Attribute("code").Value];
                        translation.GhsHazardStatement = statement;
                        statement.GhsHazardStatementTranslations.Add(translation);

                    }
                }
            }
            catch(Exception)
            {
                result = null;
            }

            return result;
        }

        
    }
}
