﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class Mixture
    {
        public int MixtureId { get; set; }

        public virtual ICollection<MixtureComponent> MixtureComponents { get; set; }

        [Required]
        [MaxLength(200)]
        public string EnglishName { get; set; }
    }
}
