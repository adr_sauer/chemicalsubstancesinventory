﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public class GhsHazardStatement
    {
        public class GhsHazardStatementByPKComparer : IEqualityComparer<GhsHazardStatement>
        {
            public bool Equals(GhsHazardStatement x, GhsHazardStatement y)
            {
                if (x == null && y == null)
                    return true;
                else if (x == null | y == null)
                    return false;
                else if (x.GhsHazardStatementId == y.GhsHazardStatementId)
                    return true;
                else
                    return false;
            }

            public int GetHashCode(GhsHazardStatement obj)
            {
                return obj.GhsHazardStatementId.GetHashCode();
            }
        }

        public int GhsHazardStatementId { get; set; }

        [Required]
        [MaxLength(20)]
        public string GhsHazardStatementCode { get; set; }

        [Required]
        [MaxLength(300)]
        public string EnglishText { get; set; }

        public bool NeedsSupplement { get; set; }

        public virtual ICollection<Substance> Substances { get; set; }

        public virtual ICollection<GhsHazardStatementTranslation> GhsHazardStatementTranslations { get; set; }

        public override string ToString()
        {
            return string.Format("Id: {0}, Code: {1}, Text: {2}", GhsHazardStatementId, GhsHazardStatementCode,
                EnglishText);
        }
    }
}
