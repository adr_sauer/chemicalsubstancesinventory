﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Services;
using ChemicalSubstancesInventory.Model.Services.Concrete;
using ChemicalSubstancesInventory.MvcAppTests.Infrastructure.MockDbContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.MvcAppTests.Model.Services
{
    [TestClass]
    public class GetSubstanceListServiceTests
    {
        [TestMethod]
        public void Should_ReturnListWith4Items_ForContextWith4Items()
        {
            //Arrange
            Substance substance1 = null;
            Substance substance2 = null;
            Substance substance3 = null;
            Substance substance4 = null;

            IChemicalInventoryContext dbContext = null;

            try
            {
                substance1 = new Substance { SubstanceId = 1, IupacName = "name1" };
                substance2 = new Substance { SubstanceId = 2, IupacName = "name2" };
                substance3 = new Substance { SubstanceId = 3, IupacName = "name3" };
                substance4 = new Substance { SubstanceId = 4, IupacName = "name4" };

                dbContext = new MockChemicalInventoryContext()
                {
                    Substances = { substance1,substance2, substance3, substance4 }
                };


            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            IGetSubstanceListService service = new GetSubstanceListService(dbContext);
            var serviceResult = service.Execute();

            //Assert
            Assert.IsNotNull(serviceResult);
            Assert.IsFalse(serviceResult.AnyErrorOccured);
            Assert.IsNotNull(serviceResult.Result);

            var result = serviceResult.Result;

            Assert.AreEqual(4, result.Count);
            Assert.IsNotNull(result.Where(q => q.IupacName == substance1.IupacName).SingleOrDefault());
            Assert.IsNotNull(result.Where(q => q.IupacName == substance2.IupacName).SingleOrDefault());
            Assert.IsNotNull(result.Where(q => q.IupacName == substance3.IupacName).SingleOrDefault());
            Assert.IsNotNull(result.Where(q => q.IupacName == substance4.IupacName).SingleOrDefault());

        }


        [TestMethod]
        public void Should_MapSubstancePropertiesToDtoProperly_ForSingleItem()
        {
            //Arrange
            Substance substance1 = null;

            IChemicalInventoryContext dbContext = null;

            try
            {
                substance1 = new Substance { SubstanceId = 1, IupacName = "name1" };

                dbContext = new MockChemicalInventoryContext()
                {
                    Substances = {substance1}
                };


            }
            catch (Exception ex)
            {
                Assert.Inconclusive(ex.Message);
            }

            //Act
            IGetSubstanceListService service = new GetSubstanceListService(dbContext);
            var serviceResult = service.Execute();

            //Assert
            Assert.IsNotNull(serviceResult);
            Assert.IsFalse(serviceResult.AnyErrorOccured);
            Assert.IsNotNull(serviceResult.Result);
            Assert.IsNotNull(serviceResult.Result.SingleOrDefault());
            var dto = serviceResult.Result.SingleOrDefault();
            Assert.AreEqual(substance1.CasNumber, dto.CasNumber);
            Assert.AreEqual(substance1.CasNumber, dto.EcNumber);
            Assert.AreEqual(substance1.CasNumber, dto.GhsSignalWordEnumValue);
            Assert.AreEqual(substance1.CasNumber, dto.IupacName);

        }


        //[TestMethod]
        //public void Should_MapPictogramPropertiesToDtoProperly_ForSingleItem()
        //{
        //    //Arrange
        //    Substance substance1 = null;
        //    GhsHazardPictogram pictogram1 = null;

        //    IChemicalInventoryContext dbContext = null;

        //    try
        //    {
        //        pictogram1 = new GhsHazardPictogram {  }
        //        substance1 = new Substance { SubstanceId = 1, IupacName = "name1" };

        //        dbContext = new MockChemicalInventoryContext();


        //    }
        //    catch (Exception ex)
        //    {
        //        Assert.Inconclusive(ex.Message);
        //    }

        //    //Act
        //    IGetSubstanceListService service = new GetSubstanceListService(dbContext);
        //    var serviceResult = service.Execute();

        //    //Assert
        //    Assert.IsNotNull(serviceResult);
        //    Assert.IsFalse(serviceResult.AnyErrorOccured);
        //    Assert.IsNotNull(serviceResult.Result);
        //    Assert.IsNotNull(serviceResult.Result.SingleOrDefault());
        //    var dto = serviceResult.Result.SingleOrDefault();
        //    Assert.AreEqual(substance1.CasNumber, dto.CasNumber);
        //    Assert.AreEqual(substance1.CasNumber, dto.EcNumber);
        //    Assert.AreEqual(substance1.CasNumber, dto.GhsSignalWordEnumValue);
        //    Assert.AreEqual(substance1.CasNumber, dto.IupacName);
        //    dto.GhsHazardPictograms.FirstOrDefault().
        //}
    }
}
