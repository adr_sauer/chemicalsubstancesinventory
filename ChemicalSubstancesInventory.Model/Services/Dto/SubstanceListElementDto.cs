﻿using ChemicalSubstancesInventory.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Dto
{
    public class SubstanceListElementDto
    {
        public string IupacName { get; set; }
        public string CasNumber { get; set; }
        public string EcNumber { get; set; }
        public GhsSignalWordEnum GhsSignalWordEnumValue { get; set; }
        public ICollection<SubstanceListHazardStatementDto> GhsHazardStatements { get; set; }
        public ICollection<SubstanceListHazardPictogramDto> GhsHazardPictograms { get; set; }
    }

    public class SubstanceListHazardStatementDto
    {
        public string GhsHazardStatementCode { get; set; }
        public string TranslatedDescription { get; set; }
    }

    public class SubstanceListHazardPictogramDto
    {
        public int GhsHazardPictogramId { get; set; }
        public string GhsPictogramCode { get; set; }
        public string SvgData { get; set; }
        public string TranslatedDescription { get; set; }
    }
}
