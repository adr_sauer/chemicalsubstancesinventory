﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChemicalSubstancesInventory.Model.Validators;

namespace ChemicalSubstancesInventory.MvcAppTests.Model.Validators
{
    [TestClass]
    public class ECNumberValidatorTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Should_ThrowArgumentNullException_IfECStringIsNull()
        {
            //Arrange
            string ecString = null;
            var validator = new ECNumberValidator();

            //Act
            bool result = validator.IsValidECNumber(ecString);

            //Assert - Expects exception
        }

        [TestMethod]
        public void Should_ReturnFalse_IfECIsEmpty()
        {
            //Arrange
            string ecString = "";
            var validator = new ECNumberValidator();

            //Act
            bool result = validator.IsValidECNumber(ecString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnFalse_IfECContainsALetter()
        {
            //Arrange
            string ecString = "215-60a-7";
            var validator = new ECNumberValidator();

            //Act
            bool result = validator.IsValidECNumber(ecString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnFalse_IfECContainsOnlyOneHyphen()
        {
            //Arrange
            string ecString = "215-6057";
            var validator = new ECNumberValidator();

            //Act
            bool result = validator.IsValidECNumber(ecString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnFalse_IfECContainsThreeHyphens()
        {
            //Arrange
            string ecString = "215-60-5-7";
            var validator = new ECNumberValidator();

            //Act
            bool result = validator.IsValidECNumber(ecString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnFalse_IfECCheckDigitIsWrong()
        {
            //Arrange
            string ecString = "215-605-6";
            var validator = new ECNumberValidator();

            //Act
            bool result = validator.IsValidECNumber(ecString);

            //Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Should_ReturnTrue_IfECIsValid()
        {
            //Arrange
            string ecString = "215-605-7";
            var validator = new ECNumberValidator();

            //Act
            bool result = validator.IsValidECNumber(ecString);

            //Assert
            Assert.IsTrue(result);
        }
    }
}
