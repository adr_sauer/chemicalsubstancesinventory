﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public class CsiServiceExceptionError : CsiServiceError
    {
        public CsiServiceExceptionError(Exception exception) : base(string.Empty)
        {
            ErrorException = exception;
        }

        public Exception ErrorException { get; private set; }

        public override string Message { get => ErrorException.Message; }
    }
}
