﻿using ChemicalSubstancesInventory.Model.Services.Dto;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services
{
    public interface IUpdateSubstanceService : ICsiServiceWithNullableResult<bool>
    {
        SubstanceEditorDto SubstanceToUpdate { get; set; }
        //bool UpdateSubstance(SubstanceEditorDto updateDto);
    }
}
