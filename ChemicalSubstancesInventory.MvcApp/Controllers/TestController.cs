﻿using ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources;
using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ChemicalSubstancesInventory.MvcApp.Controllers
{
    public partial class TestController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        public string TestImportAnnexVIExcel()
        {
            string filename = Server.MapPath("~/App_Data/AnnexVIFirst200File.xlsx");
            string returnString = string.Empty;
            var responseBuilder = new StringBuilder();

            using (IChemicalInventoryContext context = new ChemicalInventoryContext())
            {
                //var hazardStatRepo = new GhsHazardStatementAdoRepository(context);
                //var hazardPictRepo = new GhsHazardPictogramAdoRepository(context);
                //var hazardClassRepo = new HazardClassCategoryAdoRepository(context);
                //var substanceRepo = new Substance

                var dataSource = new SubstancesAnnexVIExcelDataSource(filename, context);
                //var dataSource = new SubstancesAnnexVIExcelDataSource(filename, hazardClassRepo, hazardStatRepo, hazardPictRepo, context);

                DateTime startDate = DateTime.Now;

                var result = dataSource.GetSubstances();

                TimeSpan span = DateTime.Now - startDate;
                responseBuilder.AppendLine(span.ToString());

                foreach (var error in dataSource.ErrorList)
                {
                    responseBuilder.AppendLine(string.Format("Annex index no: {0} Error message: {1}", error.IndexNo, error.ErrorMessage));
                }


                foreach (Substance s in result)
                {
                    Substance inDbSubstance = context.Substances.Where(q => q.EcNumber == s.EcNumber).FirstOrDefault();
                    if (inDbSubstance != null)
                    {
                        inDbSubstance.UpdateFromInstance(s);
                    }
                    else
                    {
                        context.Substances.Add(s);
                    }
                    context.SaveChanges();
                }


            }

            if (responseBuilder.Length == 0)
            {
                responseBuilder.AppendLine("OK");
            }

            return responseBuilder.ToString();
        }
    }
}