﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChemicalSubstancesInventory.Model.DataImporters.ImportDataSources;
using ChemicalSubstancesInventory.Model.Entities;

namespace ChemicalSubstancesInventory.Model.Entities.Configuration
{
    //DropCreateDatabaseIfModelChanges
    //DropCreateDatabaseAlways
    public class ChemicalInventoryDbInitializer : DropCreateDatabaseIfModelChanges<ChemicalInventoryContext>
    {

        protected override void Seed(ChemicalInventoryContext context)
        {

            //GhsHazardStatement ghsHazardStatement = new GhsHazardStatement()
            //{ GhsHazardStatementCode = "H200", EnglishText = "Unstable explosive", NeedsSupplement = false };
            //context.GhsHazardStatements.Add(ghsHazardStatement);

            //GhsHazardStatementTranslation ghsHazardStatementTranslation = new GhsHazardStatementTranslation()
            //{ GhsHazardStatement=ghsHazardStatement, LanguageCode="EN",Translation= "Unstable explosive" };
            //context.GhsHazardStatementTranslations.Add(ghsHazardStatementTranslation);

            //ghsHazardStatementTranslation = new GhsHazardStatementTranslation()
            //{ GhsHazardStatement = ghsHazardStatement, LanguageCode = "PL", Translation = "Materiały wybuchowe niestabilne." };
            //context.GhsHazardStatementTranslations.Add(ghsHazardStatementTranslation);

            //ghsHazardStatement = new GhsHazardStatement()
            //{ GhsHazardStatementCode = "H201", EnglishText = "Explosive; mass explosion hazard", NeedsSupplement = false };
            //context.GhsHazardStatements.Add(ghsHazardStatement);

            //ghsHazardStatementTranslation = new GhsHazardStatementTranslation()
            //{ GhsHazardStatement = ghsHazardStatement, LanguageCode = "EN", Translation = "Explosive; mass explosion hazard" };
            //context.GhsHazardStatementTranslations.Add(ghsHazardStatementTranslation);

            //ghsHazardStatementTranslation = new GhsHazardStatementTranslation()
            //{ GhsHazardStatement = ghsHazardStatement, LanguageCode = "PL", Translation = "Materiał wybuchowy; zagrożenie wybuchem masowym." };
            //context.GhsHazardStatementTranslations.Add(ghsHazardStatementTranslation);

            //ghsHazardStatement = new GhsHazardStatement()
            //{ GhsHazardStatementCode = "H202", EnglishText = "Explosive; severe projection hazard", NeedsSupplement = false };
            //context.GhsHazardStatements.Add(ghsHazardStatement);

            //ghsHazardStatementTranslation = new GhsHazardStatementTranslation()
            //{ GhsHazardStatement = ghsHazardStatement, LanguageCode = "EN", Translation = "Explosive; severe projection hazard" };
            //context.GhsHazardStatementTranslations.Add(ghsHazardStatementTranslation);




            //List<GhsHazardStatement> ghsHazardStatementList = new List<GhsHazardStatement>();
            //ghsHazardStatementList = new List<GhsHazardStatement>() {
            //    new GhsHazardStatement() { GhsHazardStatementCode = "H200", EnglishText = "Unstable explosive", NeedsSupplement = false },
            //    new GhsHazardStatement() { GhsHazardStatementCode = "H201", EnglishText = "Explosive; mass explosion hazard", NeedsSupplement = false },
            //    new GhsHazardStatement() { GhsHazardStatementCode = "H202", EnglishText = "Explosive; severe projection hazard", NeedsSupplement = false }
            //};

            

            string ghsHazardStatementsXmlPathname = @"C:\VisualStudioProjects\ASP\WorkingProjects\ChemicalSubstancesInventory\hazard_statements.xml";
            var hazardStatementsList = new GhsHazardStatementsKeminacoXmlDataSource(ghsHazardStatementsXmlPathname).GetHazardStatements();
            hazardStatementsList.ForEach(q => context.GhsHazardStatements.Add(q));
            //context.GhsHazardStatements.AddRange(hazardStatementsList);

            string ghsPrecautionaryStatementsXmlPathname = @"C:\VisualStudioProjects\ASP\WorkingProjects\ChemicalSubstancesInventory\precautionary_statements.xml";
            var precautionaryStatementsList = new GhsPrecautionaryStatementsKeminacoXmlDataSource(ghsPrecautionaryStatementsXmlPathname).GetGhsPrecautionaryStatements();
            precautionaryStatementsList.ForEach(q => context.GhsPrecautionaryStatements.Add(q));
            //context.GhsPrecautionaryStatements.AddRange(precautionaryStatementsList);

            var hazardClassCategoriesList = new HazardClassCategoriesHardcodeDataSource().GetHazardClassCategories();
            hazardClassCategoriesList.ForEach(q => context.HazardClassCategories.Add(q));
            //context.HazardClassCategories.AddRange(hazardClassCategoriesList);

            var hazardPictogramsList = new GhsHazardPictogramsHardcodeDataSource().GetGhsHazardPictograms();
            hazardPictogramsList.ForEach(q => context.GhsHazardPictograms.Add(q));
            //context.GhsHazardPictograms.AddRange(hazardPictogramsList);

            var substancesList = new SubstancesHardcodeDataSource(hazardPictogramsList, hazardStatementsList, hazardClassCategoriesList).GetSubstances();
            substancesList.ForEach(q => context.Substances.Add(q));
            //context.Substances.AddRange();
            

            base.Seed(context);
        }
    }
}
