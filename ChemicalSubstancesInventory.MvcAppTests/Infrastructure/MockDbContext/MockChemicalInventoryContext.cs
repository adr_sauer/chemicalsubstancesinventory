﻿using ChemicalSubstancesInventory.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.MvcAppTests.Infrastructure.MockDbContext
{
    public class MockChemicalInventoryContext : IChemicalInventoryContext
    {
        public MockChemicalInventoryContext()
        {
            GhsHazardStatements = new MockGhsHazardStatementSet();
            Substances = new MockSubstanceSet();
            GhsHazardPictograms = new MockGhsHazardPictogramSet();
            GhsPrecautionaryStatements = new MockGhsPrecautionaryStatementSet();
            GhsHazardStatementTranslations = new MockGhsHazardStatementTranslationSet();
            GhsPrecautionaryStatementTranslations = new MockGhsPrecautionaryStatementTranslationSet();
            HazardClasses = new MockHazardClassSet();
            HazardClassCategories = new MockHazardClassCategorySet();
            GhsHazardPictogramTranslations = new MockGhsHazardPictogramTranslationSet();
        }

        public IDbSet<GhsHazardStatement> GhsHazardStatements { get; private set; }

        public IDbSet<Substance> Substances { get; private set; }

        public IDbSet<GhsHazardPictogram> GhsHazardPictograms { get; private set; }

        public IDbSet<GhsPrecautionaryStatement> GhsPrecautionaryStatements { get; private set; }

        public IDbSet<GhsHazardStatementTranslation> GhsHazardStatementTranslations { get; private set; }

        public IDbSet<GhsPrecautionaryStatementTranslation> GhsPrecautionaryStatementTranslations { get; private set; }

        public IDbSet<HazardClass> HazardClasses { get; private set; }

        public IDbSet<HazardClassCategory> HazardClassCategories { get; private set; }

        public IDbSet<GhsHazardPictogramTranslation> GhsHazardPictogramTranslations { get; private set; }

        public void Dispose()
        {
            //Do nothing
        }

        public int SaveChanges()
        {
            return 0;
        }
    }    
}
