﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public interface ICsiServiceWithNullableResult<T> : ICsiService<CsiServiceNullableResult<T>> where T : struct
    {

    }
}
