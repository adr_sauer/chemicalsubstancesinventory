﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public class CsiServiceResult
    {
        public CsiServiceResult(ICollection<CsiServiceError> errors)
        {
            Errors = errors ?? new List<CsiServiceError>();
        }

        public ICollection<CsiServiceError> Errors { get; private set; }


        public bool AnyErrorOccured
        {
            get
            {
                return Errors.Count > 0;
            }
        }
    }
}
