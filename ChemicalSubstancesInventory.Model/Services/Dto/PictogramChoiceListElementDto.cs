﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChemicalSubstancesInventory.Model.Entities;

namespace ChemicalSubstancesInventory.Model.Services.Dto
{
    public class PictogramChoiceListElementDto 
    {
        public GhsHazardPictogram Pictogram { get; set; }
        public bool IsChosen { get; set; }

    }
}
