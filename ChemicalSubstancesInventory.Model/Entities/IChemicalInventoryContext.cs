﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Entities
{
    public interface IChemicalInventoryContext : IDisposable
    {
        IDbSet<GhsHazardStatement> GhsHazardStatements { get; }
        IDbSet<Substance> Substances { get; }
        IDbSet<GhsHazardPictogram> GhsHazardPictograms { get; }
        IDbSet<GhsPrecautionaryStatement> GhsPrecautionaryStatements { get; }
        IDbSet<GhsHazardStatementTranslation> GhsHazardStatementTranslations { get; }
        IDbSet<GhsPrecautionaryStatementTranslation> GhsPrecautionaryStatementTranslations { get; }
        IDbSet<HazardClass> HazardClasses { get; }
        IDbSet<HazardClassCategory> HazardClassCategories { get; }
        IDbSet<GhsHazardPictogramTranslation> GhsHazardPictogramTranslations { get;  }
        int SaveChanges();
    }
}
