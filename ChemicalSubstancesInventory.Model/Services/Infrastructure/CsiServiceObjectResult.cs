﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Infrastructure
{
    public class CsiServiceObjectResult<ResultType> : CsiServiceResult where ResultType : class
    {
        private ResultType _result;

        public CsiServiceObjectResult(ICollection<CsiServiceError> errors, ResultType result = null) : base(errors)
        {
            Result = result;
        }

        public ResultType Result {
            get
            {
                if(AnyErrorOccured)
                {
                    return null;
                }
                else
                {
                    return _result;
                }
            }
            private set
            {
                _result = value;
            }
        }
    }
}
