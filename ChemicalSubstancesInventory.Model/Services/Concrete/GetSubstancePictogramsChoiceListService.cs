﻿using ChemicalSubstancesInventory.Model.Entities;
using ChemicalSubstancesInventory.Model.Services.Dto;
using ChemicalSubstancesInventory.Model.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemicalSubstancesInventory.Model.Services.Concrete
{
    public class GetSubstancePictogramsChoiceListService : IGetSubstancePictogramsChoiceListService
    {


        public GetSubstancePictogramsChoiceListService(IChemicalInventoryContext dbContext)
        {
            _dbContext = dbContext;
        }

        private IChemicalInventoryContext _dbContext;

        public int SubstanceId { get; set; }

        public CsiServiceCollectionResult<PictogramChoiceListElementDto> Execute()
        {
            var errorList = new List<CsiServiceError>();
            IQueryable<PictogramChoiceListElementDto> choiceList = null;
            
            try
            {
                Substance substance = _dbContext.Substances.Find(SubstanceId);
                if (substance != null)
                {
                    var idList = substance.GhsHazardPictograms.Select(q => q.GhsHazardPictogramId);
                    choiceList = _dbContext.GhsHazardPictograms.Select(q => new PictogramChoiceListElementDto()
                    {
                        Pictogram = q,
                        IsChosen = idList.Contains(q.GhsHazardPictogramId)
                    }
                    );

                    if (choiceList == null)
                    {
                        errorList.Add(new CsiServiceError("Unable to get substance pictograms choice list."));
                    }
                }
                else
                {
                    errorList.Add(new CsiServiceError("Unable to get substance object."));
                }
            }
            catch (Exception ex)
            {               
                choiceList = null;
                errorList.Add(new CsiServiceExceptionError(ex));
            }

            return new CsiServiceCollectionResult<PictogramChoiceListElementDto>(errorList, choiceList);
        }
    }
}
